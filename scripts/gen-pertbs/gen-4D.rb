#!/usr/bin/env ruby

require "matrix"

DIM = 4
CTR_COORD = 1.0 / DIM
BASE = CTR_COORD / 2
CTR = Vector.elements([CTR_COORD] * DIM)
RGE = (-1..DIM-1)

n = 0
# not so general, the number of loops shall be DIM-1
RGE.each do |i|
    RGE.each do |j|
        RGE.each do |k|
            l = -(i+j+k)
            if RGE.include? l
                n += 1
                v = Vector[i,j,k,l] + Vector.elements([2] * DIM)
                puts "#{v.to_a}"   # [a,b,c,d] as noted in paper
            end
        end
    end
end

puts "N = #{n}"
