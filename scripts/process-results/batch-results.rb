#!/usr/bin/env ruby

require "yaml"
require "fileutils"

require_relative "./est-lambda.rb"

PLOT_WIDTH_PER_PARAMS = 100

#~ # usage
#~ USAGE = "(i) Usage:    $ #{File.basename(__FILE__)} <lambda> <pi> <2^2Delta> <mode: n|m (numerical | manual)>"
#~ puts USAGE or exit if ARGV[3].nil?

#~ # read parameters
#~ LAMBDA  = ARGV[0].to_i
#~ PI      = ARGV[1].to_i
#~ QW      = ARGV[2].to_i
#~ MODE    = ARGV[3].to_sym

LAMBDAs = [80, 128]
PIs     = [2, 3, 4, 5, 6, 7]
QWs     = [5, 10, 20]

# output dirs & files
OUT_DIR = "out_batch"

RES_BASENAME = "batch-results"
fname_dat  = RES_BASENAME + ".dat"
path_dat = File.join OUT_DIR, fname_dat

fname_png  = RES_BASENAME + ".png"
fname_tex  = RES_BASENAME + ".tex"
path_png = File.join OUT_DIR, fname_png
path_tex = File.join OUT_DIR, fname_tex

# prepare worst time
worst_time = 0

# read yaml & write into dat file
FileUtils.mkdir_p OUT_DIR
File.open(path_dat, 'w') do |file|

    file.write     "#  t_med   t_min       N   γ   l       n   κ   t  logABK  logAKS  est. λ     η_m     η_C     η_f  V_0-cr   λ   π    2^2Δ\n"
    # omitted: '        |BK|    t_br_med       |KSK|    t_ks_med'
    file.write     "# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"

    QWs.each     do |qw|
    PIs.each     do |pi|
    LAMBDAs.each do |lambda|

        # paths to files
        fname_base = "batch-results__lambda-#{lambda}_pi-#{pi}_qw-#{qw}"
        res_dir = "results-noises_numerical"

        fname_yaml = fname_base + ".yaml"
        path_yaml = File.join "..", "..", res_dir, fname_yaml

        # check yaml file
        $stderr.puts "(!) File '#{path_yaml}' does not exist." or exit unless File.file? path_yaml

        # load results
        results = YAML.load(File.read path_yaml)
        $stderr.puts "(!) Results in '#{path_yaml}' are empty." or exit if results.empty?
        # calc total BS median & min time
        results.each{|ri| ri[:bench][:time][:time_median_ms] = ri[:bench][:time][:br_time_median_ms] + ri[:bench][:time][:ks_time_median_ms] }
        results.each{|ri| ri[:bench][:time][:time_min_ms]    = ri[:bench][:time][:br_time_min_ms]    + ri[:bench][:time][:ks_time_min_ms]    }
        # calc eta_concrete
        results.each{|ri| ri[:bench][:noise][:eta_concrete]  = 300 * Math::sqrt(ri[:params][:_2Delta] * ri[:bench][:noise][:calc_V_0_concrete] + (ri[:params][:n] + 1).to_f / (48 * ri[:params][:N]**2)) * (1 << (ri[:params][:pi] + 1))}

        # restrict to eta_C under 100%
        results.select!{|ri| ri[:bench][:noise][:eta_concrete] <= 100.0 }
        if results.empty?
            $stderr.puts "(!) \e[31m\e[1mNo params with eta < 100% found\e[0m for λ = #{lambda}, π = #{pi}, 2^2Δ = #{qw}"
            next
        end

        # take the best median time
        bp = results.select{|ri| ri[:bench][:noise][:eta_concrete] <= 100.0 }.sort_by{|ri| ri[:bench][:time][:time_median_ms] }.first

        params = bp[:params]
        n_runs = bp[:bench][:n_runs]
        times  = bp[:bench][:time]
        key_sz = bp[:bench][:key_size]
        noises = bp[:bench][:noise]

        lambda_LWE  = est_lambda(params[:n], params[:logAKSn])
        lambda_RLWE = est_lambda(params[:N], params[:logABKnn])

        br_fct = times[:br_time_min_ms] / key_sz[:bsk_size] * 1000000
        ks_fct = times[:ks_time_min_ms] / key_sz[:ksk_size] * 1000000
        puts "(i) \e[32mBR factor: %5.2f (N = %4d, l = %2d)\n    KS factor: %5.2f\e[0m" % [br_fct, params[:N], params[:l], ks_fct]

        calc_V_round = (params[:n] + 1).to_f / (48 * params[:N]**2)
        eta_meas     = 300 * Math::sqrt(params[:_2Delta] * noises[:meas_V_0]          + calc_V_round) * (1 << (params[:pi] + 1))
        eta_concrete = 300 * Math::sqrt(params[:_2Delta] * noises[:calc_V_0_concrete] + calc_V_round) * (1 << (params[:pi] + 1))
        eta_formula  = 300 * Math::sqrt(params[:_2Delta] * noises[:calc_V_0_formula]  + calc_V_round) * (1 << (params[:pi] + 1))

        file.write  " %7.2f %7.2f %7d %3d %3d %7d %3d %3d %7d %7d %7.2f" % [times[:time_median_ms], times[:time_min_ms], params[:N], params[:gamma], params[:l], params[:n], params[:kappa], params[:t], params[:logABKnn], params[:logAKSn], [lambda_LWE, lambda_RLWE].min]
        file.write  " %7.1f %7.1f %7.1f %7.3f"   % [eta_meas, eta_concrete, eta_formula, params[:v0_corr]]
        # omitted: ' %11d %11.2f %11d %11.2f' % 'key_sz[:bsk_size], times[:br_time_median_ms], key_sz[:ksk_size], times[:ks_time_median_ms], '
        file.write  " %3d %3d %7d\n" % [lambda, pi, qw]

        worst_time = times[:time_median_ms] > worst_time ? times[:time_median_ms] : worst_time
    end
    end
    # separate qw's with two newlines (new dataset in gnuplot)
    file.write "\n\n"
    end
end
puts "(i) Results written to '#{path_dat}'."

# run gnuplot script
plot_width = PLOT_WIDTH_PER_PARAMS * (PIs.size + 2)
y_tics = worst_time > 100 ? 20 : 10
#TODO read n_runs
system "gnuplot -c multiplot.sh #{path_dat} #{path_png} #{plot_width} #{QWs.size}"
#~ system "gnuplot -c multiplot-tex.sh #{path_dat} #{path_tex} #{plot_width} #{QWs.size}"
puts "(i) Find plot in '#{path_png}'."
