#!/usr/bin/env gnuplot

reset

# this can be used via: $ gnuplot -c plot.sh data.dat
RES     = ARG1
OUT     = ARG2
WID     = ARG3
NRUNS   = ARG4
YTICS   = ARG5
#~ RES = 'sample.dat'
#~ OUT = 'sample.png'
#~ WID = 1800

# for fakubitsu, 1/3e6   ~ 0.33/1e6
# for argentera, 1/2.5e6 = 0.40/1e6
#~ KS_RATIO = 0.40/1e6             # good
KS_RATIO = 0.37/1e6             # good

# for argentera:
#~ BR_RATIO_1     = 0.99/1e6     # good
#~ BR_RATIO_2     = 0.65/1e6     # good
#~ BR_RATIO_3     = 0.54/1e6     # good
#~ BR_RATIO_4     = 0.49/1e6     # good
#~ BR_RATIO_5     = 0.46/1e6     # good
#~ BR_RATIO_6     = 0.43/1e6     # !!
#~ BR_RATIO_7     = 0.41/1e6     # good
#~ BR_RATIO_8     = 0.40/1e6     # good
#~ BR_RATIO_9_13  = 0.38/1e6     # !!
#~ BR_RATIO_14    = 0.365/1e6    # good
#~ BR_RATIO_15    = 0.362/1e6    # good
#~ BR_RATIO_DEF   = 0.36/1e6     # good for 16

# for argentera (updated):
BR_RATIO_1     = 1.01/1e6     # good
BR_RATIO_2     = 0.68/1e6     # good
BR_RATIO_3     = 0.56/1e6     # good
BR_RATIO_4     = 0.51/1e6     # good
BR_RATIO_5     = 0.475/1e6    # good
BR_RATIO_6     = 0.455/1e6    # !!
BR_RATIO_7     = 0.44/1e6     # good
BR_RATIO_8     = 0.425/1e6    # good
BR_RATIO_9_13  = 0.405/1e6    # !!
BR_RATIO_14    = 0.390/1e6    # good
BR_RATIO_15    = 0.385/1e6    # good
BR_RATIO_DEF   = 0.382/1e6    # good for 16

bs_ratio(l) =    l==1 ? BR_RATIO_1 : \
                (l==2 ? BR_RATIO_2 : \
                (l==3 ? BR_RATIO_3 : \
                (l==4 ? BR_RATIO_4 : \
                (l==5 ? BR_RATIO_5 : \
                (l==6 ? BR_RATIO_6 : \
                (l==7 ? BR_RATIO_7 : \
                (l==8 ? BR_RATIO_8 : \
                (l<14 ? BR_RATIO_9_13 : \
                (l==14? BR_RATIO_14 : \
                (l==15? BR_RATIO_15 : \
                         BR_RATIO_DEF))))))))))

RATIO_WID = 0.04
MAX_L = 16

set term pngcairo dashed size WID,900
set out OUT

set xtics       font ",10" rotate by 10 right right offset 8,-.5
set x2tics      font ",10"

set ytics YTICS font ",10"

set grid ytics

unset key

set style fill solid 0.6 noborder
set boxwidth 0.8

#~ set xrange [-.5:]
set yrange [0:]

#TODO resolve for multiplot
set tmargin at screen 0.23
set bmargin at screen 0.92

#~ set bars small

set title "# of runs = ".NRUNS

#                    x(row):y(t_med):err(t_med-t_min):width(l):lc:xtic
# for batch results was: plot RES index qwi ...
plot RES u 0:1:($1-$2):($5/MAX_L):($3==512?3:($3==1024?2:($3==2048?5:4))):xtic("N = ".stringcolumn(3).", γ = ".stringcolumn(4)."\nn = ".stringcolumn(6).", l = ".stringcolumn(5)."\nκ = ".stringcolumn(7).", t = ".stringcolumn(8)."\nlog(BK) = ".stringcolumn(9)."\nlog(KS) = ".stringcolumn(10)."\nλ ≈ ".stringcolumn(11)."\nη m = ".stringcolumn(16)."%\nη C = ".stringcolumn(17)."%\nη f = ".stringcolumn(18)."%\nV0-c = ".stringcolumn(19)):x2tic(stringcolumn(1)." ms") w boxerrorbars lc variable, \
      '' u ($0):($15):($5/MAX_L/2):(0) w xerr pointtype 0 lc variable, \
      '' u ($0+$5/MAX_L/2+RATIO_WID/2):($12*bs_ratio($5)+$14*KS_RATIO):(RATIO_WID):(6) w boxes lc variable, \
      '' u ($0+$5/MAX_L/2+RATIO_WID/2):($14*KS_RATIO):(RATIO_WID):(4) w boxes lc variable
