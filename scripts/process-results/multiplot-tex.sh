#!/usr/bin/env gnuplot

reset

# this can be used via: $ gnuplot -c plot.sh data.dat
RES     = ARG1
OUT     = ARG2
WID     = ARG3
QWS     = ARG4

MAX_L = 10

#~ set term pngcairo dashed size WID,700
set term epslatex size 15cm,10cm color colortext standalone font 10 header \
'\usepackage{amsmath}\usepackage{nicefrac}\usepackage[usestackEOL]{stackengine}\usepackage{xspace}\graphicspath{{..}}\newcommand{\LWE}{\ensuremath{\mathsf{LWE}}\xspace}'
set out OUT
                                  #  l   r    b   t
set multiplot layout QWS, 1 margins .05,.996,.10,.994 spacing 0,0.03

#~ set x2tics      font ",10"
set ytics 100 font ",8"
set y2tics 50

set grid ytics y2tics

#~ unset key
set key left top

set style fill solid .6 # noborder
#~ set boxwidth .8

set xrange [1.5:7.5]
set yrange [0:330]
set y2range [0:330]

#TODO resolve for multiplot
#~ set tmargin at screen 0.23
#~ set bmargin at screen 0.92

# https://stackoverflow.com/questions/38465811/gnuplot-unwanted-white-space-on-the-left-side-in-legend-key-box
set key width -3

#                        x(pi):y(t_med):width(l):lc:xtic:x2tic
do for [qwi=0:QWS-1] {
    if (qwi==QWS-1) {
        set xtics       font ",10" # rotate by 10 right right offset 8,-.5
        set title '$2^{2\Delta} = 20$' offset 0,-3
    } else {
        unset xtics
        set title '$2^{2\Delta} = '.((qwi+1)*5).'$' offset 0,-3
    }
    if (qwi>0) {
        unset key
    }
    plot \
        RES index qwi u ($17+($16==80?-.2:.2)):($12<=200?($16==80?$1:NaN):NaN):($5/MAX_L):($3==512?2:($3==1024?4:($3==2048?7:9))):xtic($16==80?'$\lambda\!\approx\!80,$':'$128$') w boxes lc variable fs solid .3 notitle, \
        RES index qwi u ($17+($16==80?-.2:.2)):($12<=200?($16==80?NaN:$1):NaN):($5/MAX_L):($3==512?2:($3==1024?4:($3==2048?7:9))) w boxes lc variable fs solid .7 notitle, \
        RES index qwi u ($17+($16==80?-.2:.2)):($12 >200?($16==80?$1:NaN):NaN):($5/MAX_L):($3==512?2:($3==1024?4:($3==2048?7:9))) w boxes lc variable fs pattern 1 notitle, \
        RES index qwi u ($17+($16==80?-.2:.2)):($12 >200?($16==80?NaN:$1):NaN):($5/MAX_L):($3==512?2:($3==1024?4:($3==2048?7:9))) w boxes lc variable fs pattern 2 notitle, \
        RES index qwi u ($17):(NaN):xtic('\Longstack[r]{~\\~\\~\\$\pi = '.stringcolumn(17).'$}') w l notitle, \
        NaN w boxes lc 2 title '$N = \phantom{1\,}512$', \
        NaN w boxes lc 4 title '$N = 1\,024$', \
        NaN w boxes lc 7 title '$N = 2\,048$', \
        NaN w boxes lc 9 title '$N = 4\,096$', \
        NaN w boxes lc rgb "#606060" fs pattern 2 title "Incorr.\ res."
}

unset multiplot
