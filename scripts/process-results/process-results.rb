#!/usr/bin/env ruby

require "yaml"
require "fileutils"

require_relative "./est-lambda.rb"

PLOT_WIDTH_PER_PARAMS = 130

# usage
USAGE = "(i) Usage:    $ #{File.basename(__FILE__)} <lambda> <pi> <2^2Delta> <mode: n|m (numerical | manual)>"
puts USAGE or exit if ARGV[3].nil?

# read parameters
LAMBDA  = ARGV[0].to_i
PI      = ARGV[1].to_i
QW      = ARGV[2].to_i
MODE    = ARGV[3].to_sym

case MODE
when :n
    MODE_STR = "numerical"
when :m
    MODE_STR = "manual"
else
    puts "(!) Unrecognized mode: #{MODE}"
    puts USAGE
    exit
end
OUT_DIR = "out_#{MODE_STR}"

# paths to files
fname_base = "results__lambda-#{LAMBDA}_pi-#{PI}_qw-#{QW}"
res_dir = "results-noises_#{MODE_STR}"

fname_yaml = fname_base + ".yaml"
path_yaml = File.join "..", "..", res_dir, fname_yaml

fname_dat  = fname_base + ".dat"
path_dat = File.join OUT_DIR, fname_dat

fname_png  = fname_base + ".png"
fname_tex  = fname_base + ".tex"
path_png = File.join OUT_DIR, fname_png
path_tex = File.join OUT_DIR, fname_tex

# check yaml file
$stderr.puts "(!) File '#{path_yaml}' does not exist." or exit unless File.file? path_yaml

# load results
results = YAML.load(File.read path_yaml)
$stderr.puts "(!) Results in '#{path_yaml}' are empty." or exit if results.empty?
# calc total BS median & min time
results.each{|ri| ri[:bench][:time][:time_median_ms] = ri[:bench][:time][:br_time_median_ms] + ri[:bench][:time][:ks_time_median_ms] }
results.each{|ri| ri[:bench][:time][:time_min_ms]    = ri[:bench][:time][:br_time_min_ms]    + ri[:bench][:time][:ks_time_min_ms]    }

# prepare worst time
worst_time = 0

# read yaml & write into dat file
FileUtils.mkdir_p OUT_DIR
File.open(path_dat, 'w') do |file|

    file.write     "#  t_med   t_min       N   γ   l       n   κ   t  logABK  logAKS  est. λ        |BK|    t_br_med       |KSK|    t_ks_med     η_m     η_C     η_f  V_0-cr\n"
    file.write     "# --------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
    #~ results.sort_by{|ri| ri[:bench][:time][:time_median_ms] }.each do |ri|
    results.each do |ri|

        params = ri[:params]
        n_runs = ri[:bench][:n_runs]
        times  = ri[:bench][:time]
        key_sz = ri[:bench][:key_size]
        noises = ri[:bench][:noise]

        lambda_LWE  = est_lambda(params[:n], params[:logAKSn])
        lambda_RLWE = est_lambda(params[:N], params[:logABKnn])

        br_fct = times[:br_time_min_ms] / key_sz[:bsk_size] * 1000000
        ks_fct = times[:ks_time_min_ms] / key_sz[:ksk_size] * 1000000
        puts "(i) \e[32mBR factor: %5.2f (N = %4d, l = %2d)\n    KS factor: %5.2f\e[0m" % [br_fct, params[:N], params[:l], ks_fct]

        calc_V_round = (params[:n] + 1).to_f / (48 * params[:N]**2)
        eta_meas     = 300 * Math::sqrt(params[:_2Delta] * noises[:meas_V_0]          + calc_V_round) * (1 << (params[:pi] + 1))
        eta_concrete = 300 * Math::sqrt(params[:_2Delta] * noises[:calc_V_0_concrete] + calc_V_round) * (1 << (params[:pi] + 1))
        eta_formula  = 300 * Math::sqrt(params[:_2Delta] * noises[:calc_V_0_formula]  + calc_V_round) * (1 << (params[:pi] + 1))

        file.write  " %7.2f %7.2f %7d %3d %3d %7d %3d %3d %7d %7d %7.2f" % [times[:time_median_ms], times[:time_min_ms], params[:N], params[:gamma], params[:l], params[:n], params[:kappa], params[:t], params[:logABKnn], params[:logAKSn], [lambda_LWE, lambda_RLWE].min]
        file.write  " %11d %11.2f %11d %11.2f %7.1f %7.1f %7.1f %7.3f\n" % [key_sz[:bsk_size], times[:br_time_median_ms], key_sz[:ksk_size], times[:ks_time_median_ms], eta_meas, eta_concrete, eta_formula, params[:v0_corr]]

        worst_time = ri[:bench][:time][:time_median_ms] > worst_time ? ri[:bench][:time][:time_median_ms] : worst_time
    end
end
puts "(i) Results written to '#{path_dat}'."

# run gnuplot script
plot_width = PLOT_WIDTH_PER_PARAMS * (results.size + 2)
y_tics = worst_time > 100 ? 5 : 2
system "gnuplot -c plot.sh #{path_dat} #{path_png} #{plot_width} #{results.first[:bench][:n_runs]} #{y_tics}"
#~ system "gnuplot -c plot-tex.sh #{path_dat} #{path_tex} #{plot_width} #{results.first[:bench][:n_runs]} #{y_tics}"
puts "(i) Find plot in '#{path_png}'."
