PATH_TO_SAGE = "/home/fakub/sources/SageMath/sage"
#~ PATH_TO_SAGE = "/home/klemsa/sources/sage-9.4-Ubuntu_20.04-x86_64/SageMath/sage"

ESTIMATOR_PLAIN_FILE = "LWE-estimator-src/LWE-estimator_plain.sage"
ESTIMATOR_PARAM_FILE = "LWE-estimator_param.sage"
ESTIMATOR_PY_PARAM_FILE = ESTIMATOR_PARAM_FILE + ".py"

LAMBDA_DB_YAML = "lwe-security-db.yaml"

INVALID_SEC = -Float::INFINITY

def est_lambda(n, stdv)
    File.write LAMBDA_DB_YAML, {}.to_yaml unless File.file? LAMBDA_DB_YAML
    h = YAML.load(File.read LAMBDA_DB_YAML)

    if h[[n,stdv]].nil?
        puts "\n(i) Running LWE Estimator on n = #{n}, -log(alpha) = #{-stdv} ... "

        # load estimator code
        est_sage = File.read ESTIMATOR_PLAIN_FILE
        # extend by provided values & estimator command
        est_sage += "\n"
        est_sage += "n, stdv = #{n}, 2^#{stdv}"
        est_sage += '
set_verbose(1)
_ = estimate_lwe(n, sqrt(2*pi)*stdv, 2^64,  reduction_cost_model=BKZ.sieve, secret_distribution=(0,1))
'

        # save to temporary file
        File.write ESTIMATOR_PARAM_FILE, est_sage

        # run Sage & get results
        res_sg = `#{PATH_TO_SAGE} #{ESTIMATOR_PARAM_FILE} 2>&1`   # výsledky na stderr, pecka..
        puts "(i) Result from Sage:\n#{res_sg}\n"

        # delete tmp files
        File.delete ESTIMATOR_PARAM_FILE    if File.exists? ESTIMATOR_PARAM_FILE
        File.delete ESTIMATOR_PY_PARAM_FILE if File.exists? ESTIMATOR_PY_PARAM_FILE

        # parse results into 2^lambda.frac
        pow = res_sg[/2\^[0-9]+\.[0-9]+/]

        sec = pow.nil? ? INVALID_SEC : pow.gsub("2^", "").to_f

        h[[n,stdv]] = sec

        unless sec == INVALID_SEC
            File.write LAMBDA_DB_YAML, h.to_yaml
            puts "(i) Result: %.2f bits (written to YAML db)." % sec
        else
            puts "(!) Result not obtained from LWE Estimator due to an error."
        end
    else
        puts "(i) Reading n = #{n}, -log(alpha) = #{stdv} from YAML db."
    end

    h[[n,stdv]]
end
