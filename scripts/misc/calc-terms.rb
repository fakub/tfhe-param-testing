#!/usr/bin/env ruby

require "yaml"

def heart(nn, n, gamma, l, kappa, t, logAKSn, logABKnn)
    #BROT_IM
    #~ 6.0*n*l*nn*2**(2*(gamma-1))*2**(2*logABKnn)
    #BROT_ORIG
    2.0*n*l*nn*2**(2*(gamma-1))*2**(2*logABKnn)
end
def club(nn, n, gamma, l, kappa, t, logAKSn, logABKnn)
    1.0*  t*nn*2**(2*(kappa-1))*2**(2*logAKSn)
end

def diamond(nn, n, gamma, l, kappa, t, logAKSn, logABKnn)
    1.0*n *(1+nn)*2**(-2*(gamma*l+1))
end
def spade(nn, n, gamma, l, kappa, t, logAKSn, logABKnn)
    1.0*nn       *2**(-2*(kappa*t+1))
end

def vround(nn, n)
    (n+1) / (48*nn**2)
end
def vmax(pow_2Delta, v0, nn, n)
    1.0 * pow_2Delta * v0 + vround(nn, n)
end
def vmax_bnd(pi)
    1.0 / (3**2 * 2**(2*(pi+1)))
end
def eta(pi, pow_2Delta, v0, nn, n)
    3.0 * Math::sqrt(pow_2Delta * v0 + vround(nn, n)) * (1 << (pi + 1))
end

# usage
USAGE = "(i) Usage:    $ #{File.basename(__FILE__)} <results.yaml>"
puts USAGE or exit if ARGV[0].nil?

FILENAME = ARGV[0]

# check yaml file
$stderr.puts "(!) File '#{FILENAME}' does not exist." or exit unless File.file? FILENAME

# load results
results = YAML.load(File.read FILENAME)
$stderr.puts "(!) Results in '#{FILENAME}' are empty." or exit if results.empty?

results.each do |ri|
    par = ri[:params]

    puts
    puts par.to_s

    h = heart par[:N], par[:n], par[:gamma], par[:l], par[:kappa], par[:t], par[:logAKSn], par[:logABKnn]
    d = diamond par[:N], par[:n], par[:gamma], par[:l], par[:kappa], par[:t], par[:logAKSn], par[:logABKnn]
    c = club par[:N], par[:n], par[:gamma], par[:l], par[:kappa], par[:t], par[:logAKSn], par[:logABKnn]
    s = spade par[:N], par[:n], par[:gamma], par[:l], par[:kappa], par[:t], par[:logAKSn], par[:logABKnn]

    #                     a  *          y
    hb = 1.0 * par[:abcd][0] * par[:xy][1] / (3**2 * 2**(2*par[:pi] + 2) * par[:_2Delta])
    db = 1.0 * par[:abcd][1] * par[:xy][1] / (3**2 * 2**(2*par[:pi] + 2) * par[:_2Delta])
    cb = 1.0 * par[:abcd][2] * par[:xy][1] / (3**2 * 2**(2*par[:pi] + 2) * par[:_2Delta])
    sb = 1.0 * par[:abcd][3] * par[:xy][1] / (3**2 * 2**(2*par[:pi] + 2) * par[:_2Delta])

    v0_calc = h + d + c + s
    v0_calc_C = ri[:bench][:noise][:calc_V_0_concrete]
    v0_meas = ri[:bench][:noise][:meas_V_0]

    vmax_calc = vmax par[:_2Delta], v0_calc, par[:N], par[:n]
    vmax_meas = vmax par[:_2Delta], v0_meas, par[:N], par[:n]

    vmax_bnd = 1.0 / (3**2 * 2**(2*par[:pi]+2))

    eta_calc = eta par[:pi], par[:_2Delta], v0_calc, par[:N], par[:n]
    eta_meas = eta par[:pi], par[:_2Delta], v0_meas, par[:N], par[:n]

    puts "(♥) %12.10f of %12.10f (used %7.2f%%, bnd ratio %7.5f, actual ratio %7.5f)" % [h, hb, h/hb*100, hb/(hb+db+cb+sb), h/(h+d+c+s)]
    puts "(♦) %12.10f of %12.10f (used %7.2f%%, bnd ratio %7.5f, actual ratio %7.5f)" % [d, db, d/db*100, db/(hb+db+cb+sb), d/(h+d+c+s)]
    puts "(♣) %12.10f of %12.10f (used %7.2f%%, bnd ratio %7.5f, actual ratio %7.5f)" % [c, cb, c/cb*100, cb/(hb+db+cb+sb), c/(h+d+c+s)]
    puts "(♠) %12.10f of %12.10f (used %7.2f%%, bnd ratio %7.5f, actual ratio %7.5f)" % [s, sb, s/sb*100, sb/(hb+db+cb+sb), s/(h+d+c+s)]
    puts "(V_0 M calc) %12.10f" % [v0_calc]
    puts "(V_0 C calc) %12.10f" % [v0_calc_C]
    puts "(V_0   meas) %12.10f" % [v0_meas]
    puts "(V_max calc) %12.10f of %12.10f (%7.2f%%)" % [vmax_calc, vmax_bnd, vmax_calc/vmax_bnd*100]
    puts "(V_max meas) %12.10f of %12.10f (%7.2f%%)" % [vmax_meas, vmax_bnd, vmax_meas/vmax_bnd*100]
    puts "(eta   calc) %12.10f" % [eta_calc]
    puts "(eta   meas) %12.10f" % [eta_meas]
    puts "--------------------------------------------------------------------------------"
end
