#!/usr/bin/env gnuplot

reset

set style fill solid noborder

bw = 2e-4
set boxwidth 3*bw/4
plot '../../results-noises/noises__lambda-90_pi-5_qw-20.dat' u (bw*floor(($1+bw/2)/bw)):(1.0) smooth freq w boxes

pause -1
