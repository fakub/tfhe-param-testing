use std::fmt;

use colored::Colorize;
use concrete::*;

use crate::misc::*;

#[cfg(feature = "numerical")]
mod gen;
mod num;
mod bench;

#[derive(Clone)]   //WISH Serialize, Deserialize (also elsewhere)
pub struct Params {
    // input params
    pub        lambda:  usize,          // desired bit-security
    pub      s_lambda:    f64,          // parameter s_lambda
    pub bit_precision:  usize,          // aka. pi
    pub   quad_weight:  usize,          // aka. 2^2Δ
    // derived TFHE params
    pub    lwe_params:  LWEParams,
    //~ #[allow(non_snake_case)]
    //~ pub       logAKSn:  f64,            // orig. f64 value of -log alpha_KS(n)
    pub   rlwe_params:  RLWEParams,
    //~ #[allow(non_snake_case)]
    //~ pub      logABKnn:  f64,            // orig. f64 value of -log alpha_BK(N)
    pub   bs_base_log:  usize,          // aka. gamma
    pub      bs_level:  usize,          // aka. l
    pub   ks_base_log:  usize,          // usually equals 1 (base = 2), now named kappa
    pub      ks_level:  usize,          // aka. t
    //~ // estimated lambda
    //~ pub    est_lambda:  f64,            // estimated bit-security
    // V_0 correction
    pub       v0_corr:  f64,
}

impl Params {

    pub fn to_yaml(&self, lvl: usize) -> String {
        let ind = "  ".repeat(lvl);
        format!("{}:lambda: {}\n{}:pi: {}\n{}:_2Delta: {}\n{}:N: {}\n{}:n: {}\n{}:k: {}\n{}:gamma: {}\n{}:l: {}\n{}:kappa: {}\n{}:t: {}\n{}:logAKSn: {}\n{}:logABKnn: {}\n{}:v0_corr: {}",   //   - :lambda_in: {}\n   ...   \n  :est_lambda: {}
            // input params
            ind, self.lambda,
            ind, self.bit_precision,
            ind, self.quad_weight,
            // derived params
            ind, self.rlwe_params.polynomial_size,
            ind, self.lwe_params.dimension,
            ind, self.rlwe_params.dimension,
            ind, self.bs_base_log,
            ind, self.bs_level,
            ind, self.ks_base_log,
            ind, self.ks_level,
            ind, self.lwe_params.log2_std_dev,
            ind, self.rlwe_params.log2_std_dev,
            ind, self.v0_corr,
            //~ // estimated lambda
            //~ self.est_lambda,
        )
    }


    // maximum and rounding error bounds

    /// V_max bound (total var err budget)
    pub fn total_var_err_budget(pi: usize) -> f64 {
        1.0 / (3*3 * (1 << (2*pi+2))) as f64
    }

    pub fn total_var_err_budget_ins(&self) -> f64 {
        Params::total_var_err_budget(self.bit_precision)
    }

    /// V_round bound
    pub fn round_err_bound(
        nu: usize,
        n: usize,
    ) -> f64 {
        (n+1) as f64 / (48_u64 * (1 << (2*nu))) as f64
    }

    pub fn v_round_ins(&self) -> f64 {
        Params::round_err_bound((self.rlwe_params.polynomial_size as f64).log2().round() as usize, self.lwe_params.dimension)
    }


    // terms (♠) .. (♥)

    /// V_(♥) bound
    pub fn heart(
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        n:          usize,
        gamma:      usize,
        l:          usize,
        log_v_bk:   f64,
    ) -> f64 {
        // (♥)
        // orig. variant w/o updated Var
        // 2^2Delta   * factor from BRot  * 2 * n * l *  N                * 2^(2(gamma-1))                *  V_BK(N)
        // (pow_2Delta * BROT_NOISE_FACTOR * 2 * n * l * (1 << nu)) as f64 * ((2*(gamma-1)) as f64).exp2() * (-2.0 * s_lambda * (1 << nu) as f64).exp2() / v0_corr
        // updated Var
        // 2^2Delta * factor from BRot  * 2 * n * l *  N                * (2^(2 gamma-1) + 1) / 6                    * V_BK(N)
        (pow_2Delta * BROT_NOISE_FACTOR * 2 * n * l * (1 << nu)) as f64 * (( (2*gamma-1) as f64).exp2() + 1.0) / 6.0 * log_v_bk.exp2()
    }

    pub fn heart_ins(&self) -> f64 {
        Params::heart(self.quad_weight, (self.rlwe_params.polynomial_size as f64).log2().round() as usize, self.lwe_params.dimension, self.bs_base_log, self.bs_level, (2 * self.rlwe_params.log2_std_dev) as f64)
    }

    /// V_(♦) bound
    pub fn diamond(
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        n:          usize,
        gamma:      usize,
        l:          usize,
    ) -> f64 {
        // (♦)
        // orig. variant w/o updated Var
        //  2^2Delta   * n * (1 +  N       )         * 2^(-2*(gamma*l+1))
        // ((pow_2Delta * n * (1 + (1 << nu))) as f64 * (-((2*(gamma*l+1)) as f64)).exp2()) / v0_corr
        // updated Var
        // 2^2Delta * n * (1 +  N       )         * 2^(-2*(gamma*l+1))                 / 3
        (pow_2Delta * n * (1 + (1 << nu))) as f64 * (-((2*(gamma*l+1)) as f64)).exp2() / 3.0
    }

    pub fn diamond_ins(&self) -> f64 {
        Params::diamond(self.quad_weight, (self.rlwe_params.polynomial_size as f64).log2().round() as usize, self.lwe_params.dimension, self.bs_base_log, self.bs_level)
    }

    /// V_(♣) bound
    pub fn club(
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        kappa:      usize,
        t:          usize,
        log_v_ks:   f64,
    ) -> f64 {
        // (♣)
        // orig. variant w/o updated Var
        // 2^2Delta   *  N        * t         * 2^(2(kappa-1))                *  V_KS(n)
        // (pow_2Delta * (1 << nu) * t) as f64 * ((2*(kappa-1)) as f64).exp2() * (-2.0 * s_lambda * n_max as f64).exp2() / v0_corr
        // updated Var
        // 2^2Delta *  N        * t         * (2^(2 kappa-1) + 1) / 6                    *  V_KS(n)
        (pow_2Delta * (1 << nu) * t) as f64 * (( (2*kappa-1) as f64).exp2() + 1.0) / 6.0 * log_v_ks.exp2()
    }

    pub fn club_ins(&self) -> f64 {
        Params::club(self.quad_weight, (self.rlwe_params.polynomial_size as f64).log2().round() as usize, self.ks_base_log, self.ks_level, (2 * self.lwe_params.log2_std_dev) as f64)
    }

    /// V_(♠) bound
    pub fn spade(
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        kappa:      usize,
        t:          usize,
    ) -> f64 {
        // (♠)
        // orig. variant w/o updated Var
        //  2^2Delta   *  N                * 2^(-2*(kappa*t+1))
        // ((pow_2Delta * (1 << nu)) as f64 * (-((2*(kappa*t+1)) as f64)).exp2()) / v0_corr
        // updated Var
        // 2^2Delta *  N                * 2^(-2*(kappa*t+1))                 / 3
        (pow_2Delta * (1 << nu)) as f64 * (-((2*(kappa*t+1)) as f64)).exp2() / 3.0
    }

    pub fn spade_ins(&self) -> f64 {
        Params::spade(self.quad_weight, (self.rlwe_params.polynomial_size as f64).log2().round() as usize, self.ks_base_log, self.ks_level)
    }

    #[allow(non_snake_case)]
    pub fn v_0_pow_2Delta_ins(&self) -> f64 {
        self.heart_ins() + self.diamond_ins() + self.club_ins() + self.spade_ins()
    }

    pub fn v_max_ins(&self) -> f64 {
        self.v_0_pow_2Delta_ins() + self.v_round_ins()
    }
}

impl fmt::Display for Params {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "    π = {}, 2^2∆ = {}, n = {}, N = {}, κ = {}, t = {}\n    γ = {}, l = {}, -log α_KS = {:.4}, -log α_BK = {:.4}, λ ≈ {}",   // λ_in = {}(est. λ ≈ {:.2})
                       self.bit_precision,
                               self.quad_weight,
                                          self.lwe_params.dimension,
                                                  self.rlwe_params.polynomial_size,
                                                          self.ks_base_log,
                                                                  self.ks_level,
                                                                              self.bs_base_log,
                                                                                      self.bs_level,
                                                                                              -self.lwe_params.log2_std_dev,
                                                                                                                 -self.rlwe_params.log2_std_dev,
                                                                                                                                    self.lambda,
                                                                                                                            //~ self.est_lambda,
        )
    }
}

impl fmt::Debug for Params {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}\n{}",
            format!("(?) Params & Error bound usage").yellow(),
            format!("{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}",
                self,
                format!("    V_(♥)   {:12.10} ({:7.2}% of V_max)",  self.heart_ins(),   self.heart_ins()    /self.v_max_ins()*100.0),
                format!("    V_(♦)   {:12.10} ({:7.2}% of V_max)",  self.diamond_ins(), self.diamond_ins()  /self.v_max_ins()*100.0),
                format!("    V_(♣)   {:12.10} ({:7.2}% of V_max)",  self.club_ins(),    self.club_ins()     /self.v_max_ins()*100.0),
                format!("    V_(♠)   {:12.10} ({:7.2}% of V_max)",  self.spade_ins(),   self.spade_ins()    /self.v_max_ins()*100.0),
                format!("    V_round {:12.10} ({:7.2}% of V_max)",  self.v_round_ins(), self.v_round_ins()  /self.v_max_ins()*100.0),
                format!("    V_max   {:12.10} ({:7.2}% of V_total; V_0-corr {:5.3})",
                                                                    self.v_max_ins(),   self.v_max_ins()    /self.total_var_err_budget_ins()*100.0, self.v0_corr),
                format!("    t_BS  ~ {:8.2} ms", self.num_bs_time_ms()),
            ),
        )
    }
}
