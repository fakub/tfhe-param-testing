use crate::params::*;

/// In Concrete, 64-bit Torus is used
pub const TORUS_SIZE_B: usize = 8;
pub const MB_TO_B:      usize = 1_000_000;

//TODO enable in setup.yaml
pub const  KS_KSK_RATIO: f64 = 0.37;    //  80  2  3
//~ pub const  KS_KSK_RATIO: f64 = 0.36;    // 128  2  3
//~ pub const  KS_KSK_RATIO: f64 = 0.40;    //  --  5 20

impl Params {

    pub fn num_bs_time_ms(&self) -> f64 {
        self.num_bk_size_MB() * self.brot_bk_ratio() + self.num_ksk_size_MB() * self.ks_ksk_ratio()
    }

    #[allow(non_snake_case)]
    fn num_bk_size_MB(&self) -> f64 {
        // 4nlN * 64bit   .. for some reason, keys in Concrete are twice as big than expected
        (8 * self.lwe_params.dimension * self.bs_level * self.rlwe_params.polynomial_size * TORUS_SIZE_B) as f64 / MB_TO_B as f64
    }

    #[allow(non_snake_case)]
    fn num_ksk_size_MB(&self) -> f64 {
        // (n + 1)tN * 64bit
        ((self.lwe_params.dimension + 1) * self.ks_level * self.rlwe_params.polynomial_size * TORUS_SIZE_B) as f64 / MB_TO_B as f64
    }

    //TODO enable in setup.yaml
    fn brot_bk_ratio(&self) -> f64 {
        //~ // for Concrete on Argentera:
        //~ match self.bs_level {
             //~ 1 => 0.99,
             //~ 2 => 0.65,
             //~ 3 => 0.54,
             //~ 4 => 0.49,
             //~ 5 => 0.46,
             //~ 6 => 0.43,
             //~ 7 => 0.41,
             //~ 8 => 0.40,
             //~ 9..=13 => 0.38,
            //~ 14 => 0.365,
            //~ 15 => 0.362,
             //~ _ => 0.36,
        //~ }
        // new measurements using Concrete on Argentera:
        match self.bs_level {
             1 => 1.01,     //
             2 => 0.68,
             3 => 0.56,
             4 => 0.51,
             5 => 0.475,
             6 => 0.455,    // ?
             7 => 0.44,
             8 => 0.425,
             9..=13 => 0.405,   // ?
            14 => 0.390,
            15 => 0.385,
             _ => 0.382,
        }
    }

    fn ks_ksk_ratio(&self) -> f64 {
        KS_KSK_RATIO
    }
}
