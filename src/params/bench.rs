use std::fs::*;

use itertools::Itertools;
use concrete::*;

use crate::params::*;
use crate::keys::*;
use crate::*;

impl Params {

    pub fn bench_p(&self, stp: &Setup, res_file: &mut File, nss_file: &mut File) -> Result<(), Box<dyn Error>> {

        let key = Keys::new(&self)?;
        // get keys's filenames & respective sizes in bytes
        let (sk_file, bsk_file, ksk_file) = Keys::filenames_from_params(&self);
        let  sk_size = fs::metadata( sk_file)?.len();
        let bsk_size = fs::metadata(bsk_file)?.len();
        let ksk_size = fs::metadata(ksk_file)?.len();

        // setup plaintext
        let m = 1;

        // setup arrays for statistics on timing & errors
        let mut br_times_us = Vec::new();
        let mut ks_times_us = Vec::new();
        let mut noises   = Vec::new();
        #[allow(non_snake_case)]
        let mut calc_V_0_concrete = 0.0;

        let mut _t: f64 = 0.0;
        simple_duration!(
            ["Bench {}× BS", stp.n_repeat],
            [
                for _ in 0..stp.n_repeat {
                    // prepare empty ciphertext for aggregation
                    // kreténi smazali zero_with_encoder a ikdyž ho sem zkopíruju, stejně je to pomrdaný ptž ani s encoderem se to neumí sečíst a vysere se to
                    let c = LWE::encrypt_uint(
                        &key.sk,
                        m,
                        &key.encoder,
                    )?;
                    //~ let mut agg = LWE::zero_with_encoder(key.sk.dimension, &key.encoder)?;

                    // aggregate fresh ciphertexts
                    //TODO vymrdaný: Number of bits of padding should be the same: 0 != 1
                    //~ for _ in 0..self.quad_weight-1 {
                        //~ // encode & encrypt
                        //~ let c = LWE::encode_encrypt(
                            //~ &key.sk,
                            //~ m,
                            //~ &key.encoder,
                        //~ )?;
                        //~ // add
                        //~ agg.add_inplace(&c)?;
                    //~ }

                    // measure & log duration of bootstrapping split into Blind Rotate & Key Switching
                    let mut br_time_us = f64::INFINITY;   // suppress output from simple_duration
                    simple_duration!(
                        ["BRot"],
                        [
                            let c_br = c.bootstrap_with_function(&key.bsk, |x| x, &key.encoder)?;
                        ],
                        br_time_us
                    );
                    br_times_us.push(br_time_us);
                    let mut ks_time_us = f64::INFINITY;   // suppress output from simple_duration
                    simple_duration!(
                        ["KSw"],
                        [
                            let b = c_br.keyswitch(&key.ksk)?;
                        ],
                        ks_time_us
                    );
                    ks_times_us.push(ks_time_us);

                    // keep noise variance (the calculated value is always the same)
                    calc_V_0_concrete = b.variance;

                    // decrypt, decode, (verify correctness) & log actual amount of noise
                    let d = b.decrypt_uint_no_rounding(&key.sk)?;
                    //~ //DBG
                    //~ println!("(?) decr_no_round = {:064b}, err in Z/2^pi Z = {}, err in Torus = {}",
                        //~ d,
                        //~ m as f64 - d as f64 / (1_u64 << (64 - self.bit_precision)) as f64,
                        //~ m as f64 / (1 << self.bit_precision) as f64 - d as f64 / (1_u64 << 32) as f64 / (1_u64 << 32) as f64,
                    //~ );

                    // m to Torus from cleartext space (div by 2^pi)
                    let m_torus = m as f64 / (self.bit_precision as f64).exp2();
                    // d to Torus from u64 representation (div by 2^64)
                    //WISH <Torus as Numeric>::BITS instead of 64
                    let d_torus = d as f64 / (64.0_f64).exp2();
                    // calc noise (consider overflow)
                    let ns = if d_torus > m_torus + 0.5 {
                        m_torus - d_torus + 1.0
                    } else {
                        // d in [0, m + 0.5] ... all fine
                        m_torus - d_torus
                    };

                    noises.push(ns);
                }
            ],
            _t
        );

        // calc time & noise statistics
        let br_time_median_ms  = median(&br_times_us) / MS_TO_US as f64;
        let br_time_min_ms     = min(&br_times_us) / MS_TO_US as f64;
        let ks_time_median_ms  = median(&ks_times_us) / MS_TO_US as f64;
        let ks_time_min_ms     = min(&ks_times_us) / MS_TO_US as f64;
        #[allow(non_snake_case)]
        let meas_V_0        = var(&noises);
        let noise_max_abs   = max_abs(&noises);

        if let Err(e) = write!(nss_file, "{:e}\n\n", noises.iter().format("\n")) {
            eprintln!("(!) Error writing to '{:?}': {}", nss_file, e.to_string());
        }

        // pre-calc variables (short names)
        let pi = self.bit_precision;
        #[allow(non_snake_case)]
        let pow_2Delta = self.quad_weight;

        // what is the goal on V_max (correctness by 3-sigma rule)
        #[allow(non_snake_case)]
        let calc_V_0_formula = self.v_0_pow_2Delta_ins() / pow_2Delta as f64;

        // calculated Var of rounding step: (n+1) / (48 N^2)
        let eta_m = 3.0 * (pow_2Delta as f64 * meas_V_0 + self.v_round_ins()).sqrt() * (1 << (pi + 1)) as f64;
        #[allow(non_snake_case)] //            ____________/   meas. V_max   \____________
        let eta_C = 3.0 * (pow_2Delta as f64 * calc_V_0_concrete + self.v_round_ins()).sqrt() * (1 << (pi + 1)) as f64;
        #[allow(non_snake_case)] //            ____________/   meas. V_max   \____________
        let eta_f = 3.0 * (pow_2Delta as f64 * calc_V_0_formula + self.v_round_ins()).sqrt() * (1 << (pi + 1)) as f64;

        //DBG
        println!("(?) meas_V_0:          {:10.8} | {:5.2}\n    calc_V_0_concrete: {:10.8} | {:5.2}\n    calc_V_0_formula:  {:10.8} | {:5.2}\n    calc_V_round:      {:10.8} | {:5.2}\n    calc_V_max_formula:{:10.8} | {:5.2}\n    V_total_budget:    {:10.8} | {:5.2}\n    meas_noise_max_abs:{:10.8} | {:5.2}\n    3σ_meas   / E_max: {:8.2}% (aka η_m)\n    3σ_calc_C / E_max: {:8.2}% (aka η_C; using calc V_0 Concrete)\n    3σ_calc_F / E_max: {:8.2}% (aka η_f; using calc V_0 formula)",
            meas_V_0,           -meas_V_0.log2(),
            calc_V_0_concrete,  -calc_V_0_concrete.log2(),
            calc_V_0_formula,   -calc_V_0_formula.log2(),
            self.v_round_ins(), -self.v_round_ins().log2(),
            self.v_max_ins(),   -self.v_max_ins().log2(),
            self.total_var_err_budget_ins(),    -self.total_var_err_budget_ins().log2(),
            noise_max_abs,      -noise_max_abs.log2(),
            eta_m * 100 as f64,
            eta_C * 100 as f64,
            eta_f * 100 as f64,
        );

        // write params & results
        if let Err(e) = write!(res_file, "- :params:\n{}\n  :bench:\n    :n_runs: {}\n    :time:\n      :br_time_median_ms: {}\n      :br_time_min_ms: {}\n      :ks_time_median_ms: {}\n      :ks_time_min_ms: {}\n    :key_size:\n      :sk_size: {}\n      :bsk_size: {}\n      :ksk_size: {}\n    :noise:\n      :meas_V_0: {}\n      :calc_V_0_concrete: {}\n      :calc_V_0_formula: {}\n      :noise_max_abs: {}\n",
            self.to_yaml(2),
            stp.n_repeat,
            br_time_median_ms,
            br_time_min_ms,
            ks_time_median_ms,
            ks_time_min_ms,
            sk_size,
            bsk_size,
            ksk_size,
            meas_V_0,
            calc_V_0_concrete,
            calc_V_0_formula,
            noise_max_abs,
        ) {
            eprintln!("(!) Error writing to '{:?}': {}", res_file, e.to_string());
        }

        Ok(())
    }
}
