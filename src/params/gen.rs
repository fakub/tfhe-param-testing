#[allow(unused_imports)]
use colored::Colorize;

use crate::params::*;
#[cfg(feature = "manual")]
use crate::manual_params::*;
use crate::setup::*;

impl Params {

    /// Generate params grouped by inputs: lambda, pi, 2Delta
    pub fn gen_all(stp: &Setup) -> Vec<Vec<Params>> {

        // vector of groups of params
        let mut all_params = Vec::new();

        // loop input parameters
        for &lambda_s_lambda in &stp.lambda_s_lambdas {
            for &pi_2_delta in &stp.pi_pow_2deltas {

                println!("{}", format!("\n(i) Generating {}-bit params for π = {}, 2^2∆ = {}.",
                    lambda_s_lambda.0, pi_2_delta.0, pi_2_delta.1,
                ).bold().green());

                // group of params for lambda, pi, 2Delta
                let mut par_grp = Vec::new();

                // setup some basic variables
                let   lambda = lambda_s_lambda.0;
                let s_lambda = lambda_s_lambda.1;
                let n_min = 2 * lambda;
                let pi = pi_2_delta.0;
                #[allow(non_snake_case)]
                let pow_2Delta = pi_2_delta.1;
                #[allow(non_snake_case)]
                let Delta2 = (pow_2Delta as f64).log2();

                // loop v0_corr
                for &v0_corr in &stp.v0_corr {

                    let mut par_v0_corr_subgrp = Vec::new();

                    // =============================================================
                    //  Generation Algorithm BEGIN
                    //

                    // loop N
                    for nu in stp.nu_min..=stp.nu_max {

                        // calc N
                        #[allow(non_snake_case)]
                        let N = 1 << nu;

                        // calc n_max
                        //~ let n_max = ((N as f64).powf(2.0) / (3.0 * (1 << (2*pi-2)) as f64) - 1.0).floor() as usize;

                        //TODO check if this is OK
                        //TODO check n_max > n_min
                        let n_max = (((N as f64).powf(2.0) / (3.0 * (1 << (2*pi-2)) as f64) - 1.0).floor() as usize).min(N);

                        // get global minimum on kappa·t from (♠)
                        let kappa_t_min = (Params::spade_kappa_t_min(pi, Delta2, nu, v0_corr)).ceil() as usize;

                        // set initial kappa
                        let mut kappa = 1;

                        #[cfg(feature = "verb")]
                        println!("{}", format!("####    N = {:4} with κt_min = {}    ####", N, kappa_t_min).green().bold());

                        // check if round and (♣) is feasible with t = 1 (weak condition -- minimum noise from n_max, no (♠) noise, rounding noise from n_min)
                        while Params::club_round_max_err_budget(pi, pow_2Delta, nu, kappa, 1, s_lambda, n_min, n_max, v0_corr) > 0.0 {

                            #[cfg(feature = "verb")]
                            println!("{}", format!("====    κ = {}    ====", kappa).cyan().bold());

                            // set initial t (int div ceil)
                            let mut t = (kappa_t_min - 1) / kappa + 1;
                            if t < 1 {break;}

                            // check if round and (♣) leave some error budget with actual t
                            // n.b.! (♠) must not be considered, otherwise t might not start to grow
                            while Params::club_round_max_err_budget(pi, pow_2Delta, nu, kappa, t, s_lambda, n_min, n_max, v0_corr) > 0.0 && t <= stp.t_max {

                                // loop n s.t. log(alpha_KS) is an integer (n.b., this is just a limitation of Concrete v0.1.11, it can be smoother)
                                #[allow(non_snake_case)]
                                let neg_log_alpha_KS_min = (s_lambda * n_min as f64).ceil() as usize;
                                #[allow(non_snake_case)]
                                let neg_log_alpha_KS_max = (s_lambda * n_max as f64).floor() as usize;

                                #[cfg(feature = "verb")]
                                println!("{}", format!("----    t = {} with -log(α_KS) = {} .. {}    ----", t, neg_log_alpha_KS_min, neg_log_alpha_KS_max).blue());

                                #[allow(non_snake_case)]
                                for neg_log_alpha_KS in neg_log_alpha_KS_min..=neg_log_alpha_KS_max {

                                    // calc n from -log(alpha_KS)
                                    let n = (neg_log_alpha_KS as f64 / s_lambda).ceil() as usize;

                                    // get actual minimum on gamma·l from (♦) using remaining error budget
                                    let gamma_l_min = (Params::diamond_gamma_l_min(pi, pow_2Delta, nu, kappa, t, s_lambda, n, v0_corr)).ceil() as usize;

                                    // set initial gamma
                                    let mut gamma = 1;

                                    #[cfg(feature = "verb")]
                                    println!("{}", format!("        n = {:4} with γl_min = {}", n, gamma_l_min).purple());

                                    // check if (♥) is feasible with l = 1 (weak condition -- no (♦) noise)
                                    while Params::heart_spade_club_round_max_err_budget(pi, pow_2Delta, nu, kappa, t, s_lambda, n, gamma, 1, v0_corr) > 0.0 {

                                        #[cfg(feature = "verb")]
                                        println!("{}", format!("            γ = {}", gamma).purple());

                                        // set initial l (int div ceil)
                                        let mut l = (gamma_l_min - 1) / gamma + 1;
                                        if l < 1 {break;}

                                        // check if round, (♠), (♣) and (♥) leave some error budget with actual l
                                        // n.b.! (♦) must not be considered, otherwise l might not start to grow
                                        while Params::heart_spade_club_round_max_err_budget(pi, pow_2Delta, nu, kappa, t, s_lambda, n, gamma, l, v0_corr) > 0.0 && l <= stp.l_max {

                                            #[cfg(feature = "verb")]
                                            println!("{}", format!("                l = {}", l).purple());

                                            if Params::final_err_budget(pi, pow_2Delta, nu, kappa, t, s_lambda, n, gamma, l, v0_corr) >= 0.0 {

                                                #[cfg(feature = "verb")]
                                                println!("{}", format!("                    SUCCESS!!").green());

                                                // SUCCESS!! add params
                                                //TODO check neg_log_alpha_KS vs. TORUS_PREC
                                                let par = Params {
                                                    lambda,
                                                    s_lambda,
                                                    bit_precision: pi,
                                                    quad_weight: pow_2Delta,
                                                    lwe_params: LWEParams {
                                                        dimension: n,
                                                        log2_std_dev: -(neg_log_alpha_KS as i32),
                                                    },
                                                    rlwe_params: RLWEParams {
                                                        polynomial_size: N,
                                                        dimension: 1,
                                                        // this way, error is greater => better security, but further from 99.7% decrypt success rate (which is usually not a problem)
                                                        log2_std_dev: (-s_lambda * N as f64).ceil() as i32,
                                                    },
                                                    bs_base_log: gamma,
                                                    bs_level: l,
                                                    ks_base_log: kappa,
                                                    ks_level: t,
                                                    v0_corr,
                                                };

                                                //~ println!("{:?}", par);

                                                par_v0_corr_subgrp.push(par);
                                            }

                                            // increase l
                                            l += 1;
                                        }

                                        // increase gamma
                                        gamma += 1;
                                    }

                                    #[cfg(feature = "verb")]
                                    println!("{}", "            No other γ feasible due to (♥)".purple());
                                }

                                // increase t
                                t += 1;
                            }

                            #[cfg(feature = "verb")]
                            println!("{}", format!("----    No other t feasible due to (♣) or t exceeded T_MAX = {}", stp.t_max).blue());

                            // increase kappa
                            kappa += 1;
                        }

                        #[cfg(feature = "verb")]
                        println!("{}", "====    No other κ feasible due to (♣)    ====".cyan().bold());
                    }
                    //
                    //  Generation Algorithm END
                    // =============================================================

                    // refine params (remove "useless" params)
                    // DEPRECATED: Params::refine(&mut par_v0_corr_subgrp);

                    // ---------------------------------------------------------
                    //  Take the "best" params only BEGIN
                    //

                    // sort params by BS time
                    par_v0_corr_subgrp.sort_by(|p,q| p.num_bs_time_ms().partial_cmp(&q.num_bs_time_ms()).unwrap() );

                    //DBG
                    println!("(?) {} parameters for V_O corr = {} (before draining)", par_v0_corr_subgrp.len(), v0_corr);

                    // take few best bootstrapping times (some may achieve equal timings & different error rates, may take more params)
                    let mut del_from_idx = 0;
                    let mut bs_time_groups = 0;
                    let mut last_bs_time = 0.0;
                    for (i, p) in par_v0_corr_subgrp.iter().enumerate() {
                        // hitting new (N,n ; l,t) group (new BS time)
                        if p.num_bs_time_ms() != last_bs_time {
                            if bs_time_groups >= stp.n_top_bs_times {
                                del_from_idx = i;
                                break;
                            }
                            bs_time_groups += 1;
                            last_bs_time = p.num_bs_time_ms();
                        }
                    }
                    par_v0_corr_subgrp.drain(del_from_idx..);

                    // for each BS time, take only few parameters sorted by V_max
                    // save BS time groups
                    last_bs_time = 0.0;
                    let mut start_idx = 0;
                    let mut bst_grps = Vec::new();
                    for (i, p) in par_v0_corr_subgrp.iter().enumerate() {
                        // hitting new (N,n ; l,t) group (new BS time)
                        if p.num_bs_time_ms() != last_bs_time {
                            // add previous BS time group (if i == 0, no previous group)
                            if i > 0 {
                                bst_grps.push(par_v0_corr_subgrp[start_idx..i].to_vec());   // equvalent to ..=i-1
                            }
                            //~ println!("(?) slice[{}..{}-1] ...", start_idx, i);
                            // refresh
                            start_idx = i;
                            last_bs_time = p.num_bs_time_ms();
                        }
                    }
                    // save last BS time group
                    if par_v0_corr_subgrp.len() > 0 {
                        bst_grps.push(par_v0_corr_subgrp[start_idx..par_v0_corr_subgrp.len()].to_vec());   // equiv to ..=pgr.len()-1
                        //~ println!("(?) slice[{}..{}-1] ...", start_idx, par_v0_corr_subgrp.len());
                    }

                    // sort & take the best ones by V_max
                    let mut par_cnt = 0;
                    for bst_grp in &mut bst_grps {
                        // sort by V_max
                        bst_grp.sort_by(|p,q| p.v_max_ins().partial_cmp(&q.v_max_ins()).unwrap() );
                        // take N_TOP_V_MAXES best
                        if bst_grp.len() > stp.n_top_v_maxes {
                            bst_grp.drain(stp.n_top_v_maxes..);
                        }
                        // update count
                        par_cnt += bst_grp.len();
                    }

                    //DBG
                    println!("(?) {} parameters for V_O corr = {} (after double-draining)", par_cnt, v0_corr);

                    // add subgroup to group
                    for mut bst_grp in &mut bst_grps {
                        par_grp.append(&mut bst_grp);
                    }

                    //
                    //  Take the "best" params only END
                    // ---------------------------------------------------------

                } // loop V_0 corrections

                // deduplicate params
                Params::dedupl(&mut par_grp);

                //DBG
                println!("(?) {} parameters (after deduplication across V_0 corr's)", par_grp.len());
                for p in &par_grp {
                    println!("{:?},    V_0-corr = {},    t_BS ~ {:6.2} ms,    eta ~ {:5.1}%\n----", p, p.v0_corr, p.num_bs_time_ms(), p.v_max_ins()/p.total_var_err_budget_ins()*100.0);
                }

                println!("{}", format!("\n(i) Generated {} valid {}-bit params for π = {}, 2^2∆ = {}.\n\n################################################################################\n",
                    par_grp.len(), lambda_s_lambda.0, pi_2_delta.0, pi_2_delta.1,
                ).bold().green());

                // add group to all params vector
                all_params.push(par_grp);
            }
        }

        all_params
    }


    /// DEPRECATED: now, keep more gamma's/kappa's than just one (affects error, not just speed)
    /// Keep relevant params (N, l; n, t) only with lowest gamma, kappa
    /// Since they grow, always keep only the first occurence
    //~ fn refine(pars: &mut Vec<Params>) {
        //~ let mut relevant = vec![];

        //~ pars.retain(|par| {
            //~ #[allow(non_snake_case)]
            //~ let N = par.rlwe_params.polynomial_size;
            //~ let l = par.bs_level;
            //~ let n = par.lwe_params.dimension;
            //~ let t = par.ks_level;

            //~ if relevant.contains(&(N, l, n, t)) {
                //~ // relevant quadruple already exists in pars => delete
                //~ return false; // delete
            //~ } else {
                //~ // new relevant quadruple => update & keep
                //~ relevant.push((N, l, n, t));
                //~ return true; // keep
            //~ }
        //~ });
    //~ }


    // deduplicate params

    /// Deduplicate params (already grouped with different V_0 corrections, taken only few best BS times)
    fn dedupl(pars: &mut Vec<Params>) {
        let mut relevant = vec![];

        pars.retain(|par| {
            #[allow(non_snake_case)]
            let N = par.rlwe_params.polynomial_size;
            let n = par.lwe_params.dimension;
            let gamma = par.bs_base_log;
            let l     = par.bs_level;
            let kappa = par.ks_base_log;
            let t     = par.ks_level;

            if relevant.contains(&(N, n, gamma, l, kappa, t)) {
                // relevant quadruple already exists in pars => delete
                return false; // delete
            } else {
                // new relevant quadruple => update & keep
                relevant.push((N, n, gamma, l, kappa, t));
                return true; // keep
            }
        });
    }


    // auxiliary functions

    /// Get minimum on kappa·t from spade (♠)
    fn spade_kappa_t_min(
        pi:         usize,
        #[allow(non_snake_case)]
        Delta2:     f64,
        nu:         usize,
        v0_corr:    f64,
    ) -> f64 {
        // orig. variant w/o updated Var
        // ((2*pi + nu) as f64 + 2.0 * THREE.log2() + Delta2 - v0_corr.log2()) / 2.0
        //TODO add round noise
        // updated Var: spade divided by 3
        ((2*pi + nu) as f64 + 1.0 * THREE.log2() + Delta2 - v0_corr.log2()) / 2.0
    }

    /// Calculate upper bound on remaining error budget after rounding and (♣), ignore (♠)
    fn club_round_max_err_budget(
        pi:         usize,
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        kappa:      usize,
        t:          usize,
        s_lambda:   f64,
        n_min:      usize,
        n_max:      usize,
        v0_corr:    f64,
    ) -> f64 {
        let log_v_ks = -2.0 * s_lambda * n_max as f64;

        // total
        Params::total_var_err_budget(pi)
        // round
        - Params::round_err_bound(nu, n_min)
        // (♣)
        - Params::club(pow_2Delta, nu, kappa, t, log_v_ks) / v0_corr
    }

    /// Calculate remaining error budget after rounding, (♠) and (♣)
    fn spade_club_round_err_budget(
        pi:         usize,
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        kappa:      usize,
        t:          usize,
        s_lambda:   f64,
        n:          usize,
        v0_corr:    f64,
    ) -> f64 {
        let log_v_ks = -2.0 * s_lambda * n as f64;

        // total
        Params::total_var_err_budget(pi)
        // round
        - Params::round_err_bound(nu, n)
        // (♣)
        - Params::club(pow_2Delta, nu, kappa, t, log_v_ks) / v0_corr
        // (♠)
        - Params::spade(pow_2Delta, nu, kappa, t) / v0_corr
    }

    /// Get minimum on gamma·l from diamond (♦) using error budget after rounding, (♠) and (♣)
    #[allow(non_snake_case)]
    fn diamond_gamma_l_min(
        pi:         usize,
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        kappa:      usize,
        t:          usize,
        s_lambda:   f64,
        n:          usize,
        v0_corr:    f64,
    ) -> f64 {
        //                    err budget after rounding, (♠) and (♣)
        let to_log2 = Params::spade_club_round_err_budget(pi, pow_2Delta, nu, kappa, t, s_lambda, n, v0_corr)
        // orig. variant w/o updated Var
        //     2^2Delta   * n * (1 +  N       )
        // / ((pow_2Delta * n * (1 + (1 << nu))) as f64 / v0_corr);
        // updated Var
        //  2^2Delta   * n * (1 +  N       )         / 3
        / ((pow_2Delta * n * (1 + (1 << nu))) as f64 / 3.0 / v0_corr);

        // get gamma·l
        -to_log2.log2()/2.0 - 1.0
    }

    /// Calculate upper bound on remaining error budget after rounding, (♠), (♣) and (♥), ignore (♦)
    fn heart_spade_club_round_max_err_budget(
        pi:         usize,
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        kappa:      usize,
        t:          usize,
        s_lambda:   f64,
        n:          usize,
        gamma:      usize,
        l:          usize,
        v0_corr:    f64,
    ) -> f64 {
        let log_v_bk = -2.0 * s_lambda * (1 << nu) as f64;
        let log_v_ks = -2.0 * s_lambda *  n        as f64;

        // total
        Params::total_var_err_budget(pi)
        // round
        - Params::round_err_bound(nu, n)
        // (♣)
        - Params::club(pow_2Delta, nu, kappa, t, log_v_ks) / v0_corr
        // (♠)
        - Params::spade(pow_2Delta, nu, kappa, t) / v0_corr
        // (♥)
        - Params::heart(pow_2Delta, nu, n, gamma, l, log_v_bk) / v0_corr
    }

    /// Calculate final remaining error budget
    fn final_err_budget(
        pi:         usize,
        #[allow(non_snake_case)]
        pow_2Delta: usize,
        nu:         usize,
        kappa:      usize,
        t:          usize,
        s_lambda:   f64,
        n:          usize,
        gamma:      usize,
        l:          usize,
        v0_corr:    f64,
    ) -> f64 {
        let log_v_bk = -2.0 * s_lambda * (1 << nu) as f64;
        let log_v_ks = -2.0 * s_lambda *  n        as f64;

        // total
        Params::total_var_err_budget(pi)
        // round
        - Params::round_err_bound(nu, n)
        // (♣)
        - Params::club(pow_2Delta, nu, kappa, t, log_v_ks) / v0_corr
        // (♠)
        - Params::spade(pow_2Delta, nu, kappa, t) / v0_corr
        // (♥)
        - Params::heart(pow_2Delta, nu, n, gamma, l, log_v_bk) / v0_corr
        // (♦)
        - Params::diamond(pow_2Delta, nu, n, gamma, l) / v0_corr
    }
}
