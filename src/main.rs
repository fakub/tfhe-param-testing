use std::error::Error;
use std::path::Path;
use std::fs::{self,File};
use std::io::Write;

#[allow(unused_imports)]
use colored::Colorize;

extern crate chrono;
#[macro_use]
extern crate lazy_static;
extern crate yaml_rust;

mod params;
mod keys;
#[cfg(feature = "manual")]
mod manual_params;
mod setup;
mod misc;

#[cfg(feature = "numerical")]
use params::*;
#[cfg(feature = "manual")]
use manual_params::*;
use setup::*;

// resolve mutually exclusive features
#[cfg(all(feature = "manual", feature = "numerical"))]
compile_error!("Features 'manual' and 'numerical' cannot be enabled at the same time.");

// setup paths, filenames, ...
#[cfg(feature = "numerical")]
const RESULTS_NOISES_PATH: &str = "./results-noises_numerical";
#[cfg(feature = "manual")]
const RESULTS_NOISES_PATH: &str = "./results-noises_manual";

const RESULTS_FILE_BASE:   &str = "results__";
const RESULTS_FILE_EXT:    &str = "yaml";
const  NOISES_FILE_BASE:   &str = "noises__";
const  NOISES_FILE_EXT:    &str = "dat";

fn main() -> Result<(), Box<dyn Error>> {

    // load setup
    //TODO take setup filename as argument
    let stp = Setup::new("setup.yaml")?;

    // generate all params
    #[cfg(feature = "numerical")]
    let all_pars = Params::gen_all(&stp);
    #[cfg(feature = "manual")]
    let all_pars = MAN_PARAMS;

    //TODO tell how long time did the whole fun take
    // run benchmark
    for (par_gi, par_grp) in all_pars.iter().enumerate() {
        // skip empty
        if par_grp.is_empty() {continue;}

        // print info
        let lambda = par_grp[0].lambda; let pi = par_grp[0].bit_precision; let qw = par_grp[0].quad_weight;
        #[cfg(feature = "manual")]
        let task_str = "Benchmarking manual";
        #[cfg(feature = "numerical")]
        let task_str = "Num. estimating";
        println!("{}", format!("\n(i) {} {}-bit params with π = {}, 2^2∆ = {}    ({}/{})\n",
            task_str, lambda, pi, qw, par_gi+1, all_pars.len(),
        ).bold().blue());

        // derive results/noises filenames
        let res_filename: String = format!("{}/{}lambda-{}_pi-{}_qw-{}.{}", RESULTS_NOISES_PATH, RESULTS_FILE_BASE, lambda, pi, qw, RESULTS_FILE_EXT);
        let nss_filename: String = format!("{}/{}lambda-{}_pi-{}_qw-{}.{}", RESULTS_NOISES_PATH,  NOISES_FILE_BASE, lambda, pi, qw, NOISES_FILE_EXT );

        // init results/noises files: delete if exist, create new
        let mut res_file;
        if Path::new(&res_filename).exists() { fs::remove_file(&res_filename)?; }
        res_file = File::create(&res_filename).expect("File::create results failed.");
        // init YAML array in results file
        if let Err(e) = write!(res_file, "---\n") {
            eprintln!("(!) Error writing to '{}': {}", res_filename, e.to_string());
        }

        let mut nss_file;
        if Path::new(&nss_filename).exists() { fs::remove_file(&nss_filename)?; }
        nss_file = File::create(&nss_filename).expect("File::create noises failed.");

        // (A) bench numerically generated parameters
        #[cfg(feature = "numerical")]
        for (par_i, par) in par_grp.iter().enumerate() {   // .take(N_TOP_NUM_BENCH)
            println!("{}", format!("\n(i) {} variant {}/{} in group {}/{}\n",
                task_str, par_i+1, par_grp.len(), par_gi+1, all_pars.len(),
            ).purple());
            println!("{:?}\n", par);
            par.bench_p(&stp, &mut res_file, &mut nss_file)?;
        }
        //~ if let Some(num_best) = par_grp.iter().min_by(|p,q| p.num_bs_time_ms().partial_cmp(&q.num_bs_time_ms()).unwrap() ) {
            //~ println!("{:?}\n", num_best);
            //~ num_best.bench_p(&stp, &mut res_file, &mut nss_file)?;
        //~ } else {
            //~ eprintln!("(!) Minimum search by numerically estimated BS time failed.\n");
        //~ }

        // (B) bench manually provided parameters
        #[cfg(feature = "manual")]
        for (par_i, par) in par_grp.iter().enumerate() {
            println!("{}", format!("\n(i) {} variant {}/{} in group {}/{}\n",
                task_str, par_i+1, par_grp.len(), par_gi+1, all_pars.len(),
            ).purple());
            println!("{:?}\n", par);
            par.bench_p(&stp, &mut res_file, &mut nss_file)?;
        }

        println!("{}", format!("\n(i) {} finished for {}-bit params with π = {}, 2^2∆ = {}\n    Results written to '{}'.\n    Noises written to '{}'.\n\n    ============================================================================\n",
            task_str, lambda, pi, qw, res_filename, nss_filename,
        ).bold().blue());
    }

    Ok(())
}
