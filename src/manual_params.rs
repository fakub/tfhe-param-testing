use concrete::*;

use crate::params::*;


// =============================================================================
//
//  Manual Params
//
pub const MAN_PARAMS: [[Params; 3]; 4] = [

// -----------------------------------------------------------------------------
//  Binary TFHE: 2|2
    [
        // 80-bit (new)
        Params {
            lambda:         80,
            s_lambda:       0.0,
            bit_precision:  2,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      316,
                log2_std_dev:   -12,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    512,
                dimension:          1,
                log2_std_dev:       -19,
            },
            bs_base_log:    5,
            bs_level:       3,
            ks_base_log:    2,
            ks_level:       4,
            v0_corr:        0.0,
        },
        // 128-bit (new)
        Params {
            lambda:         128,
            s_lambda:       0.0,
            bit_precision:  2,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      520,
                log2_std_dev:   -12,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    1024,
                dimension:          1,
                log2_std_dev:       -23,
            },
            bs_base_log:    7,
            bs_level:       2,
            ks_base_log:    2,
            ks_level:       5,
            v0_corr:        0.0,
        },
        // 128-bit (orig. by TFHE Library)
        Params {
            lambda:         128,
            s_lambda:       0.0,
            bit_precision:  2,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      630,
                log2_std_dev:   -15,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    1024,
                dimension:          1,
                log2_std_dev:       -25,
            },
            bs_base_log:    7,
            bs_level:       3,
            ks_base_log:    2,
            ks_level:       8,
            v0_corr:        0.0,
        },
    ],

// -----------------------------------------------------------------------------
//  Parmesan: 5|20
    [
        // 80-bit (new)
        Params {
            lambda:         80,
            s_lambda:       0.0,
            bit_precision:  5,
            quad_weight:    20,
            lwe_params: LWEParams {
                dimension:      474,
                log2_std_dev:   -18,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    1024,
                dimension:          1,
                log2_std_dev:       -38,
            },
            bs_base_log:    19,
            bs_level:       1,
            ks_base_log:    3,
            ks_level:       5,
            v0_corr:        0.0,
        },
        // 128-bit (new)
        Params {
            lambda:         128,
            s_lambda:       0.0,
            bit_precision:  5,
            quad_weight:    20,
            lwe_params: LWEParams {
                dimension:      747,
                log2_std_dev:   -18,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    2048,
                dimension:          1,
                log2_std_dev:       -49,
            },
            bs_base_log:    24,
            bs_level:       1,
            ks_base_log:    3,
            ks_level:       5,
            v0_corr:        0.0,
        },
        // 112-bit (orig. by Parmesan Lib.)
        Params {
            lambda:         112,
            s_lambda:       0.0,
            bit_precision:  5,
            quad_weight:    20,
            lwe_params: LWEParams {
                dimension:      680,
                log2_std_dev:   -18,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    1024,
                dimension:          1,
                log2_std_dev:       -29,
            },
            bs_base_log:    7,
            bs_level:       3,
            ks_base_log:    1,
            ks_level:       16,
            v0_corr:        0.0,
        },
    ],

// -----------------------------------------------------------------------------
//  Z/8Z Demo: 5|2
    [
        // 80-bit (new)
        Params {
            lambda:         80,
            s_lambda:       0.0,
            bit_precision:  5,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      422,
                log2_std_dev:   -16,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    1024,
                dimension:          1,
                log2_std_dev:       -38,
            },
            bs_base_log:    19,
            bs_level:       1,
            ks_base_log:    3,
            ks_level:       4,
            v0_corr:        0.0,
        },
        // 128-bit (new)
        Params {
            lambda:         128,
            s_lambda:       0.0,
            bit_precision:  5,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      712,
                log2_std_dev:   -17,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    1024,
                dimension:          1,
                log2_std_dev:       -24,
            },
            bs_base_log:    6,
            bs_level:       3,
            ks_base_log:    2,
            ks_level:       8,
            v0_corr:        0.0,
        },
        // 128-bit (orig. by Zama)
        Params {
            lambda:         128,
            s_lambda:       0.0,
            bit_precision:  5,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      750,
                log2_std_dev:   -18,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    1024,
                dimension:          1,
                log2_std_dev:       -25,
            },
            bs_base_log:    7,
            bs_level:       3,
            ks_base_log:    2,
            ks_level:       7,
            v0_corr:        0.0,
        },
    ],

// -----------------------------------------------------------------------------
//  Z/16Z Demo: 6|2
    [
        // 80-bit (new)
        //TODO FIXME
        Params {
            lambda:         80,
            s_lambda:       0.0,
            bit_precision:  6,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      500,
                log2_std_dev:   -19,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    2048,
                dimension:          1,
                log2_std_dev:       -77,
            },
            bs_base_log:    38,
            bs_level:       1,
            ks_base_log:    5,
            ks_level:       3,
            v0_corr:        0.0,
        },
        // 128-bit (new)
        Params {
            lambda:         128,
            s_lambda:       0.0,
            bit_precision:  6,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      747,
                log2_std_dev:   -18,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    2048,
                dimension:          1,
                log2_std_dev:       -49,
            },
            bs_base_log:    24,
            bs_level:       1,
            ks_base_log:    3,
            ks_level:       5,
            v0_corr:        0.0,
        },
        // 128-bit (orig. by Zama)
        Params {
            lambda:         128,
            s_lambda:       0.0,
            bit_precision:  6,
            quad_weight:    2,
            lwe_params: LWEParams {
                dimension:      750,
                log2_std_dev:   -18,
            },
            rlwe_params: RLWEParams {
                polynomial_size:    2048,
                dimension:          1,
                log2_std_dev:       -52,
            },
            bs_base_log:    7,
            bs_level:       3,
            ks_base_log:    2,
            ks_level:       7,
            v0_corr:        0.0,
        },
    ],
];


//~ #[cfg(feature = "manual")]
//~ pub const MAN_PARAMS: [[Params; 2]; 1] = [

//~ // -----------------------------------------------------------------------------
//~ //  Parmesan: 5|20
    //~ [
        //~ // 112-bit (new)
        //~ Params {
            //~ lambda:         112,
            //~ s_lambda:       0.0,
            //~ bit_precision:  5,
            //~ quad_weight:    20,
            //~ lwe_params: LWEParams {
                //~ dimension:      657,
                //~ log2_std_dev:   -18,
            //~ },
            //~ rlwe_params: RLWEParams {
                //~ polynomial_size:    1024,
                //~ dimension:          1,
                //~ log2_std_dev:       -28,
            //~ },
            //~ bs_base_log:    7,
            //~ bs_level:       3,
            //~ ks_base_log:    3,
            //~ ks_level:       5,
            //~ v0_corr:        0.0,
        //~ },
        //~ // 112-bit (orig. by Parmesan Lib.)
        //~ Params {
            //~ lambda:         112,
            //~ s_lambda:       0.0,
            //~ bit_precision:  5,
            //~ quad_weight:    20,
            //~ lwe_params: LWEParams {
                //~ dimension:      680,
                //~ log2_std_dev:   -18,
            //~ },
            //~ rlwe_params: RLWEParams {
                //~ polynomial_size:    1024,
                //~ dimension:          1,
                //~ log2_std_dev:       -29,
            //~ },
            //~ bs_base_log:    7,
            //~ bs_level:       3,
            //~ ks_base_log:    1,
            //~ ks_level:       16,
            //~ v0_corr:        0.0,
        //~ },
    //~ ],
//~ ];


//  80-bit, pi = 2, qw = 3   (for ternary gates in binary TFHE)
    //~ [
    //~ ],


//~ /// Trivial Paramter Set
//~ #[allow(dead_code)]
//~ pub const PARMXX__TRIVIAL: Params = Params {
           //~ lambda: 5,
         //~ s_lambda: 0.42, // 112: 0.0275; 80: 0.0380; 128: 0.0235
    //~ bit_precision: 2,
      //~ quad_weight: 2,
    //~ lwe_params: LWEParams {
        //~ dimension: 64,
        //~ log2_std_dev: -8,
    //~ },
    //~ rlwe_params: RLWEParams {
        //~ polynomial_size: 256,
        //~ dimension: 1,
        //~ log2_std_dev: -10,
    //~ },
    //~ bs_base_log: 2,
    //~ bs_level:    2,
    //~ ks_base_log: 1,
    //~ ks_level:    3,
    //~ v0_corr:   0.0,
//~ };

//~ /// TFHE Parameter Set A (90-bit security)
//~ #[allow(dead_code)]
//~ pub const PARM90__PI_2__D_02__A: Params = Params {
           //~ lambda: 90,
         //~ s_lambda: 0.42, // 112: 0.0275; 80: 0.0380; 128: 0.0235
    //~ bit_precision: 3,   // other params correspond with pi = 2, one more bit is needed to simulate logic operations, which work with halves
      //~ quad_weight: 2,
       //~ lwe_params: LWEParams {
        //~ dimension: 400,
     //~ log2_std_dev: -13, // -13.31,
    //~ },
    //~ rlwe_params: RLWEParams {
        //~ polynomial_size: 1024,
              //~ dimension: 1,
           //~ log2_std_dev: -31, // -31.20,
    //~ },
    //~ bs_base_log: 15,
       //~ bs_level: 1,
    //~ ks_base_log: 1,
       //~ ks_level: 11,
        //~ v0_corr: 0.0,
//~ };

//~ /// TFHE Parameter Set B (90-bit security)
//~ #[allow(dead_code)]
//~ pub const PARM90__PI_2__D_03__B: Params = Params {
           //~ lambda: 90,
         //~ s_lambda: 0.42, // 112: 0.0275; 80: 0.0380; 128: 0.0235
    //~ bit_precision: 3,   // other params correspond with pi = 2, one more bit is needed to simulate logic operations, which work with halves
      //~ quad_weight: 3,
       //~ lwe_params: LWEParams {
        //~ dimension: 420,
     //~ log2_std_dev: -13, // -13.61,
    //~ },
    //~ rlwe_params: RLWEParams {
        //~ polynomial_size: 1024,
              //~ dimension: 1,
           //~ log2_std_dev: -32, // -32.53,
    //~ },
    //~ bs_base_log: 16,
       //~ bs_level: 1,
    //~ ks_base_log: 1,
       //~ ks_level: 11,
        //~ v0_corr: 0.0,
//~ };
