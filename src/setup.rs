use std::error::Error;
use std::path::Path;
use std::fs;

use yaml_rust::YamlLoader;   // YamlEmitter

#[derive(Clone,Debug)]
pub struct Setup {
    // experiment setup
    pub lambda_s_lambdas:   Vec<(usize, f64)>,
    pub pi_pow_2deltas:     Vec<(usize, usize)>,
    pub v0_corr:            Vec<f64>,

    // experiment parameters
    pub n_repeat:           usize,
    pub n_top_bs_times:     usize,
    pub n_top_v_maxes:      usize,

    // param ranges
    pub nu_min:             usize,
    pub nu_max:             usize,
    pub t_max:              usize,
    pub l_max:              usize,
}


// =============================================================================
//
//  Default Setup
//
lazy_static! {
    pub static ref SETUP_DEF: Setup = Setup {

        // -------------------------------------------------------------------------
        //  Experiment Setup
        //
        /// Security Parameter(s) (numerical mode only)
        ///
        /// - ( 90, 0.0335) (new; was 0.0328)
        /// - (112, 0.0275) (new; was 0.0271)
        /// - ( 80, 0.0380)
        /// - (128, 0.0235)
        ///
        /// [(128, 0.0235)];
        /// [(112, 0.0274)];
        /// [( 80, 0.0380)];
        /// [( 80, 0.0380), (127, 0.0238), (128, 0.0235)];
        /// alternative s_lambda's
        /// [(125, 0.0241)];
        /// [(129, 0.0231)];
        /// [(126, 0.0239)];
        lambda_s_lambdas: vec![( 80, 0.0380)],

        /// Plaintext Precision(s), Quadratic Weight(s) (numerical mode only)
        /// [(2,  2)];   // TFHE Lib addition
        /// [(2,  3)];   // Binary addition with ternary gates
        /// [(5,  2)];   // Demo Z/8Z  by Zama
        /// [(6,  2)];   // Demo Z/16Z by Zama
        /// [(5, 20)];   // Parmesan
        /// [(2, 2), (5, 20), (5, 2), (6, 2)];
        pi_pow_2deltas: vec![(5, 2)],

        /// V_0 correction (numerical mode only)
        /// When this is set 1.0, it has no effect
        /// [1.0, 1.1, 1.2, 1.3, 1.5, 1.75, 2.0, 2.3, 2.7, 3.2, 3.8, 4.5];
        /// [0.826, 0.909, 1.0, 1.1, 1.210, 1.331, 1.464, 1.611, 1.772, 1.949]; //
        /// [0.621, 0.683, 0.751, 0.826];   // used for 126|5|2
        /// [1.0, 1.2, 1.44, 1.73, 2.07, 2.49];
        /// [1.0];
        v0_corr: vec![1.0],


        // -------------------------------------------------------------------------
        // Experiment Parameters
        //
        /// Number of Benchmark Repetitions
        n_repeat: 500,
        /// Number of top BS times to be considered in single V_0 corrections search (numerical mode only)
        n_top_bs_times: 2,
        /// Number of top V_max'es to be considered per BS time (numerical mode only)
        n_top_v_maxes: 3,


        // -------------------------------------------------------------------------
        //  Param Ranges (numerical mode only)
        //
        nu_min: 8,
        nu_max: 11,
        t_max:  10,
        l_max:  6,
    };
}


// =============================================================================
//
//  Load Setup from YAML file
//
impl Setup {

    /// Load experiment setup (or use default values)
    pub fn new(setup_filename:  &str) -> Result<Setup, Box<dyn Error>> {
        // create Setup struct with default values
        let mut stp = SETUP_DEF.clone();

        // check if setup file exists
        if Path::new(setup_filename).is_file() {
            println!("\n(i) Loading setup from '{}' ...", setup_filename);

            // load YAML struct
            let yaml_str = fs::read_to_string(setup_filename)?;
            let docs = YamlLoader::load_from_str(&yaml_str)?;
            let setup_map = &docs[0];

            // update setup fields, if they exist in YAML

            // -------------------------------------------------------------------------
            //  Experiment Setup
            //
            if let Some(lsl_vec_yaml) = setup_map["lambda_s_lambdas"].as_vec() {
                let mut lambda_s_lambdas = vec![];
                for lsl_ary_yaml in lsl_vec_yaml {
                    if let Some(lsl_ary_fy) = lsl_ary_yaml.as_vec() {
                        if lsl_ary_fy.len() == 2 {
                            if let Some(lambda) = lsl_ary_fy[0].as_i64() {
                                if let Some(s_lambda) = lsl_ary_fy[1].as_f64() {
                                    lambda_s_lambdas.push((lambda as usize, s_lambda));
                                }
                            }
                        }
                    }
                }
                if lambda_s_lambdas.is_empty() {
                    eprintln!("(!) No valid lambda/s_lambda pairs found under 'lambda_s_lambdas' key in setup YAML file!");
                }
                stp.lambda_s_lambdas = lambda_s_lambdas;
            }
            if let Some(pd_vec_yaml) = setup_map["pi_pow_2deltas"].as_vec() {
                let mut pi_pow_2deltas = vec![];
                for pd_ary_yaml in pd_vec_yaml {
                    if let Some(pd_ary_fy) = pd_ary_yaml.as_vec() {
                        if pd_ary_fy.len() == 2 {
                            if let Some(pi) = pd_ary_fy[0].as_i64() {
                                if let Some(pow_2delta) = pd_ary_fy[1].as_i64() {
                                    pi_pow_2deltas.push((pi as usize, pow_2delta as usize));
                                }
                            }
                        }
                    }
                }
                if pi_pow_2deltas.is_empty() {
                    eprintln!("(!) No valid pi/pow_2delta pairs found under 'pi_pow_2deltas' key in setup YAML file!");
                }
                stp.pi_pow_2deltas = pi_pow_2deltas;
            }
            if let Some(v0c_vec_yaml) = setup_map["v0_corr"].as_vec() {
                let mut v0_corr = vec![];
                for v0c_yaml in v0c_vec_yaml {
                    if let Some(v0c) = v0c_yaml.as_f64() {
                        v0_corr.push(v0c);
                    }
                }
                stp.v0_corr = v0_corr;
            }

            // -------------------------------------------------------------------------
            // Experiment Parameters
            //
            // it was like this:
            // stp.n_repeat = setup_map["n_repeat"].as_i64().unwrap() as usize;
            // but it panics if the output is None
            if let Some(n_repeat) = setup_map["n_repeat"].as_i64() {
                stp.n_repeat = n_repeat as usize;
            }
            if let Some(n_top_bs_times) = setup_map["n_top_bs_times"].as_i64() {
                stp.n_top_bs_times = n_top_bs_times as usize;
            }
            if let Some(n_top_v_maxes) = setup_map["n_top_v_maxes"].as_i64() {
                stp.n_top_v_maxes = n_top_v_maxes as usize;
            }

            // -------------------------------------------------------------------------
            //  Param Ranges (numerical mode only)
            //
            if let Some(nu_min) = setup_map["nu_min"].as_i64() {
                stp.nu_min = nu_min as usize;
            }
            if let Some(nu_max) = setup_map["nu_max"].as_i64() {
                stp.nu_max = nu_max as usize;
            }
            if let Some(t_max) = setup_map["t_max"].as_i64() {
                stp.t_max = t_max as usize;
            }
            if let Some(l_max) = setup_map["l_max"].as_i64() {
                stp.l_max = l_max as usize;
            }

            println!("    {:?}", stp);

        } else {
            eprintln!("(!) Setup file '{}' does not exist.", setup_filename);
        }

        Ok(stp)
    }
}
