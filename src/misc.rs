use std::iter::Sum;


// =============================================================================
//
//  Constants
//
#[allow(dead_code)]
pub const  S_TO_MS: usize = 1000;
#[allow(dead_code)]
pub const MS_TO_US: usize = S_TO_MS;
#[allow(dead_code)]
pub const  S_TO_US: usize = S_TO_MS * MS_TO_US;

/// Noise growth factor in Blind Rotate. Differs for improved variant (3x higher noise).
#[cfg(not(feature = "brot_im"))]
pub const BROT_NOISE_FACTOR: usize = 1;
#[cfg(feature = "brot_im")]
pub const BROT_NOISE_FACTOR: usize = 3;

#[cfg(feature = "numerical")]
pub const THREE: f64 = 3.0;     // to call log2() on
//~ /// Torus Precision in Concrete
//~ #[cfg(feature = "numerical")]
//~ pub const TORUS_PREC: f64 = 64.0;   // https://github.com/zama-ai/concrete/blob/concrete-0.1.11/concrete/src/lib.rs#L21


// =============================================================================
//
//  Statistical Calculations
//

/// Mean
#[allow(dead_code)]
pub fn mean<'a, T: 'a + Into<f64>>(v: &'a Vec<T>) -> f64 where f64: Sum<&'a T> {
    v.iter().sum::<f64>() / v.len() as f64
}

/// Var
//WISH ne a ne a ne, i kdyby ses tam posral...
//~ pub fn var<'a, T: 'a + Into<f64> + std::ops::Sub<Output = f64>>(v: &'a Vec<T>) -> f64 where f64: Sum<&'a T> {
    //~ let a = mean::<T>(v);
    //~ mean::<T>(v.iter().map(|e| (e - a).powf(2.0) ).collect::<f64>())
//~ }
#[allow(dead_code)]
pub fn var(v: &Vec<f64>) -> f64 {
    let a = mean(v);
    let d: Vec<f64> = v.iter().map(|e| (e - a).powf(2.0) ).collect();
    mean(&d)
}

/// Median
#[allow(dead_code)]
pub fn median(v: &Vec<f64>) -> f64 {
    let mut vc = v.clone();
    vc.sort_by(|a, b| a.partial_cmp(b).unwrap());
    if v.len() & 1 == 1 {
        vc[vc.len()/2]
    } else {
        (vc[vc.len()/2 - 1] + vc[vc.len()/2]) / 2 as f64
    }
}

/// Min
#[allow(dead_code)]
pub fn min(v: &Vec<f64>) -> f64 {
    v.iter().fold(f64::INFINITY, |a, &b| a.min(b))
}

/// Max
#[allow(dead_code)]
pub fn max(v: &Vec<f64>) -> f64 {
    v.iter().fold(f64::NEG_INFINITY, |a, &b| a.max(b))
}

/// Max of Abs
#[allow(dead_code)]
pub fn max_abs(v: &Vec<f64>) -> f64 {
    v.iter().map(|e| e.abs() ).fold(f64::NEG_INFINITY, |a, b| a.max(b))
}


// =============================================================================
//
//  Macros
//

// Credit: Demo Z/8Z by Zama (https://github.com/zama-ai/demo_z8z ; improved)
// This macro allows to compute the duration of the execution of the expressions enclosed.
// Note that the variables are not captured.
#[macro_export]
macro_rules! simple_duration {
    ([$($msg_args:tt)*], [$($code_block:tt)+], $time:ident) => {
        let __utc_start: chrono::DateTime<chrono::Utc>;
        let __now: std::time::Instant;
        let __msg: String;
        // print msg
        __msg = format!($($msg_args)*);

        __utc_start = chrono::Utc::now();
        if $time != f64::INFINITY {
            println!(" {}  [{}.{:03}] {} ...", String::from("+").green().bold(), __utc_start.format("%M:%S"), __utc_start.timestamp_subsec_millis(), __msg);
        }
        // start timer
        __now = std::time::Instant::now();

        // run block of code
        $($code_block)+

        // get elapsed time
        let __time = __now.elapsed().as_micros() as f64;
        let __utc_end = chrono::Utc::now();
        let __s_time = if __time < 1_000. {
            String::from(format!("{} μs", __time             )).purple()
        } else if __time < 1_000_000. {
            String::from(format!("{} ms", __time / 1_000.    )).blue()
        } else {
            String::from(format!("{:.3} s",  __time / 1_000_000.)).cyan().bold()
        };
        // print result
        if $time != f64::INFINITY {
            println!(" {}  [{}.{:03}] {} (in {})\n", String::from("—").red().bold(), __utc_end.format("%M:%S"), __utc_end.timestamp_subsec_millis(), __msg, __s_time);
        }

        // save elapsed time
        $time = __time;
    }
}
