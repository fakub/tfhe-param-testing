use std::error::Error;
use std::path::Path;

use concrete::*;

use crate::params::*;
use crate::*;

pub const KEYS_PATH: &str = "./keys/";

pub struct Keys {
    // keys
    pub  sk: LWESecretKey,
    pub bsk: LWEBSK,
    pub ksk: LWEKSK,
    // encoders
    pub encoder: Encoder,
}

impl Keys {

    /// Load or generate a TFHE key set
    pub fn new(par: &Params) -> Result<Keys, Box<dyn Error>> {
        // derive filenames
        let (sk_file, bsk_file, ksk_file) = Keys::filenames_from_params(par);

        // check if the keys exist
        if     Path::new( sk_file.as_str()).is_file()
            && Path::new(bsk_file.as_str()).is_file()
            && Path::new(ksk_file.as_str()).is_file()
        {
            // load keys from files
            let mut _t: f64 = 0.0;
            simple_duration!(
                ["Load Existing Keys"],
                [
                    let keys = Keys {
                         sk: LWESecretKey::load( sk_file.as_str())?,
                        bsk:       LWEBSK::load(bsk_file.as_str()),     // does not return Result enum
                        ksk:       LWEKSK::load(ksk_file.as_str()),     // does not return Result enum
                        encoder:   Keys::get_encoder(par)?,
                    };
                ],
                _t
            );

            return Ok(keys);
        } else {
            // generate & save keys
            let mut _t: f64 = 0.0;
            simple_duration!(
                ["Generate & Save New Keys"],
                [
                    let keys = Keys::generate(par)?;

                    simple_duration!(
                        ["Saving  LWE secret key"],
                        [keys .sk.save( sk_file.as_str())?;],
                        _t);
                    simple_duration!(
                        ["Saving bootstrapping keys"],
                        [keys.bsk.save(bsk_file.as_str());],
                        _t);
                    simple_duration!(
                        ["Saving key-switching keys"],
                        [keys.ksk.save(ksk_file.as_str());],
                        _t);
                ],
                _t
            );

            return Ok(keys);
        }
    }

    /// Get filenames from params
    pub fn filenames_from_params(par: &Params) -> (String, String, String) {
        let suffix = format!("n-{}_N-{}_gamma-{}_l-{}_kappa-{}_t-{}.key",
                                par.lwe_params.dimension,
                                     par.rlwe_params.polynomial_size,
                                              par.bs_base_log,
                                                   par.bs_level,
                                                            par.ks_base_log,
                                                                 par.ks_level,
        );
        let  sk_file = format!( "{}/SK__{}", KEYS_PATH, suffix);
        let  bk_file = format!( "{}/BK__{}", KEYS_PATH, suffix);
        let ksk_file = format!("{}/KSK__{}", KEYS_PATH, suffix);

        (sk_file, bk_file, ksk_file)
    }

    /// Generate a fresh TFHE key set
    fn generate(par: &Params) -> Result<Keys, Box<dyn Error>> {
        // generate LWE & RLWE secret keys
        let mut _t: f64 = 0.0;
        simple_duration!(
            ["Generating  LWE secret key ({}-bit)", par.lwe_params.dimension],
            [let      sk:  LWESecretKey =  LWESecretKey::new(&par.lwe_params );],
            _t);
        simple_duration!(
            ["Generating RLWE secret key (deg = {})", par.rlwe_params.polynomial_size],
            [let rlwe_sk: RLWESecretKey = RLWESecretKey::new(&par.rlwe_params);],
            _t);

        // calculate bootstrapping & key-switching keys
        simple_duration!(
            ["Calculating bootstrapping keys"],
            [let bsk: LWEBSK = LWEBSK::new(
                &sk,
                &rlwe_sk,
                par.bs_base_log,
                par.bs_level,
            );],
            _t);
        simple_duration!(
            ["Calculating key-switching keys"],
            [let ksk: LWEKSK = LWEKSK::new(
                &rlwe_sk.to_lwe_secret_key(),
                &sk,
                par.ks_base_log,
                par.ks_level,
            );],
            _t);

        // fill & return Keys struct
        Ok(Keys {
            sk,     // shortand when variables and fields have the same name
            bsk,    // https://doc.rust-lang.org/book/ch05-01-defining-structs.html#using-the-field-init-shorthand-when-variables-and-fields-have-the-same-name
            ksk,
            encoder: Keys::get_encoder(par)?,
        })
    }

    /// Get some Encoder
    fn get_encoder(par: &Params) -> Result<Encoder, Box<dyn Error>> {
        Ok(Encoder::new_rounding_context(
            0.,                                                 // min
            ((1 << par.bit_precision) - 1) as f64,              // max
            par.bit_precision,                                  // bit-precision
            0,                                                  // padding
            true,                                               // negacyclic?
        )?)
        //~ Ok(Encoder::new_rounding_context(
            //~ 0.,                     // min
            //~ 1.,                     // max
            //~ par.bit_precision,      // bit-precision
            //~ 1,                      // padding
        //~ )?)
    }
}
