
# TFHE Parameter Testing Suite

This tool serves for a (semi-)automatic TFHE parameter generation.
Read more in our [whitepaper](./assets/abstract.pdf) (also on [eprint:2022/1315](https://eprint.iacr.org/2022/1315)) or check our [poster](./assets/fhe.org-conf-poster.pdf) presented at [1st Annual FHE.org Conference](https://fhe.org/conference/fhe-org-conference-2022) in Trondheim, May 2022. [NOTE: due to an incorrect QR code in the "Motivation Appendix: Parallel Arithmetics" presented there, we give the correct link here: [https://github.com/fakub/parmesan](https://github.com/fakub/parmesan)]

**Requirements**: `rust` compiler, `ruby` interpreter, `gnuplot`, recommended OS is Ubuntu 20.04 or similar.


## Gather & Setup Input Parameters

Depending on the target application of TFHE, three input parameters must be provided to our tool:

  - security level $\lambda$,
  - cleartext space bit-precision $\pi$, and
  - parameterized bound on the number of homomorphic addition/scalar multiplication operations before the sample gets bootstrapped, denoted by $2^{2\Delta}$.

These, and a couple of other parameters, can be set up in [`setup.yaml`](./setup.yaml), which can be initialized as a copy of [`setup.yaml.template`](./setup.yaml.template).


### Security Level $\lambda$

With chosen security level $\lambda$, one must also provide an estimated value of $s_\lambda$, which parameterizes a relation between LWE's noise stddev $\alpha$ and its dimension $n$, given security level $\lambda$:

$$ -\log(\alpha) \approx s_\lambda n $$

E.g., for $\lambda = 128$, the estimated value is $s_\lambda = 0.0235$.

The pair of constants $(\lambda, s_\lambda)$ shall be set in the `lambda_s_lambdas` field of [`setup.yaml`](./setup.yaml).


### Cleartext Bit-Precision $\pi$

The desired bit-size the cleartext space shall be given as the first element in the `pi_pow_2deltas` field of [`setup.yaml`](./setup.yaml).


### Homomorphic Operations Bound

Explained in the whitepaper, the bound denoted by $2^{2\Delta}$ is defined as the upper bound on the sum of squares of integer weights used during homomorphic addition/scalar multiplication before bootstrapping refreshes the noise:

$$ 2^{2\Delta} = \sum w_i^2 $$

where $w_i$ are respective integer weights of a homomorphic sum $\sum w_i c_i$ of samples $c_i$.

The parameter $2^{2\Delta}$ shall be given as the second element in the `pi_pow_2deltas` field of [`setup.yaml`](./setup.yaml).


## Generate the Params!

Other experiment parameters can be set in `setup.yaml` (number of runs, number of top parameter sets considered; explained in the whitepaper).

In the search mode, run the generation tool with

    $ make run-numerical

In the manual mode, provide your parameters in the `MAN_PARAMS` constant in [`src/manual_params.rs`](.src/manual_params.rs) and run the generation tool with

    $ make run-manual

The results are written into the folder [`results-noises_numerical`](./results-noises_numerical) (TODO might be needed to be created manually before running the tool) into a YAML file `results_lambda-<lambda>_pi-<pi>_qw-<2^2Delta>.yaml`, where the parameter sets are sorted by the expected time from the fastest.
In this file, many parameters are written (self-explanatory).


## Process Results

To visualize the parameters obtained, navigate to [`scripts/process-results`](./scripts/process-results) and run

    $ ./process-results.rb <lambda> <pi> <2^2Delta> <mode: n|m (numerical | manual)>

The results can be found in the [`out_numerical`](./scripts/process-results/out_numerical) folder.


### How It Looks

In the resulting graph, one can find the best parameters of her/his choice!
N.b., an important parameter denoted by $\eta$ expresses the percentage of the maximum error bound consumed by $3\sigma$ of the noise.

![results](./assets/results.png)


## Acknowledgements

This work was supported by the MESRI-BMBF French-German joint project UPCARE (ANR-20-CYAL-0003-01).
