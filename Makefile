build-numerical-verb:
	RUSTFLAGS="-C target-cpu=native" cargo build --release --no-default-features --features "numerical-verb"
build-numerical:
	RUSTFLAGS="-C target-cpu=native" cargo build --release --no-default-features --features "numerical"
build-manual:
	RUSTFLAGS="-C target-cpu=native" cargo build --release --no-default-features --features "manual"
run-numerical-verb:
	RUSTFLAGS="-C target-cpu=native" cargo run   --release --no-default-features --features "numerical-verb"
run-numerical:
	RUSTFLAGS="-C target-cpu=native" cargo run   --release --no-default-features --features "numerical"
run-manual:
	RUSTFLAGS="-C target-cpu=native" cargo run   --release --no-default-features --features "manual"
